<?php

$dir = __DIR__ . '/app';

$iterator = Symfony\Component\Finder\Finder::create()
    ->files()
    ->name('*.php')
    ->exclude('docs')
    ->exclude('tests')
    ->in($dir);

$options = [
    'theme' => 'default',
    'title' => '',
    'build_dir' => __DIR__ . '/storage/app/public/docs',
    'cache_dir' => __DIR__ . '/cache',
];

$sami = new Sami\Sami($iterator, $options);

return $sami;