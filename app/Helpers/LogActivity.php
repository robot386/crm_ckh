<?php

namespace App\Helpers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Request;
use App\LogActivity as LogActivityModel;


class LogActivity
{
    public static function addToLog($subject)
    {
        self::autoDeleteOfOutDatedRecord();

        $log = [];

        $log['subject'] = $subject;
        $log['url'] = Request::fullUrl();
        $log['method'] = Request::method();
        $log['ip'] = Request::ip();
        $log['agent'] = Request::header('user-agent');
        $log['user_id'] = auth()->check() ? auth()->user()->id : 1;

        LogActivityModel::create($log);
    }

    public static function logActivityLists()
    {
        return LogActivityModel::latest()->get();
    }

    public static function getLogByParam($data, $time = 1)
    {
        Carbon::setLocale('pl');
        $date = Carbon::now()->subMinutes($time)->toDateTimeString();

        $query = LogActivityModel::query();

        foreach ($data as $key => $value) {
            $query->where($key, $value);
        }

        return $query->where('updated_at', '>=' , $date)->get();
    }

    private static function autoDeleteOfOutDatedRecord($timeOutMinutes = 15)
    {
        Carbon::setLocale('pl');
        $date = Carbon::now()->subMinutes($timeOutMinutes)->toDateTimeString();

        LogActivityModel::where('updated_at', '<=' , $date)->delete();
    }
}