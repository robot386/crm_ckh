<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_id',
        'garage_id',
        'valuation_id',
        'land_register',
        'plot_number',
        'repairs_price',
    ];

    /**
     * Get the customer for the property.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    /**
     * Get the garage for the property.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function garage()
    {
        return $this->belongsTo(Garage::class);
    }

    /**
     * Get the valuation for the property.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function valuation()
    {
        return $this->belongsTo(Valuation::class);
    }

    /**
     * Get the valuation file for the property.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function fileValuation()
    {
        return $this->belongsTo(FileValuation::class);
    }
}
