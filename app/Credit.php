<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Credit extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_id',
        'purpose_id',
        'amount',
        'amount_own',
        'description',
        'property_description',
        'payment_date',
        'loan_period',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    public $sortable = [
        'payment_date'
    ];

    /**
     * Get the customer for the credit.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    /**
     * Get the purpose for the credit.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function purpose()
    {
        return $this->belongsTo(Purpose::class);
    }

}
