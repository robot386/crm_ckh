<?php

namespace App\Http\Controllers;

use App\Bank;
use App\Customer;
use App\CustomerProcess;
use App\FileValuation;
use App\Garage;
use App\Helpers\LogActivity;
use App\ModePurchase;
use App\Process;
use App\Purpose;
use App\Session;
use App\Tranche;
use App\User;
use App\Valuation;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Jenssegers\Agent\Agent;

class CustomerProcessController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * It displays a list of bank processes for the customer
     * and a form with customer data, as well as a client
     * loan partner form and a list of preferred banks.
     *
     * @param $customerId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($customerId)
    {
        LogActivity::addToLog('Wejście na stronę edycji procesu kretytobiorcy.');
        $message = $this->checkIfSameUserEditThisPage($customerId);

        $selectBanks = [];
        $paidTime = $colorClass = $fileValuation = null;

        $processes = CustomerProcess::where('customer_id', $customerId)
            ->orderBy('order', 'asc')
            ->get();

        $customer = Customer::findOrFail($customerId);

        $partners = Customer::where('customer_id', $customerId)->get();

        if ($customer->property) {
            $fileValuation = FileValuation::where('property_id', $customer->property->id)->get();
        }

        $tranche = Tranche::where('customer_id', $customerId)->first();

        $modePurchase = ModePurchase::pluck('name', 'id');
        $garage = Garage::pluck('name', 'id');
        $valuations = Valuation::pluck('name', 'id');
        $purposes = Purpose::pluck('name', 'id');
        $banks = Bank::pluck('name', 'id');

        foreach ($processes as $select) {
            $selectBanks[] = $select->bank_id;
        }

        $newBanks = Bank::selectNewBank($banks, $selectBanks);

        if ($customer->credit) {
            $colorClass = $this->changePaymentDate($customer->credit->payment_date)['color'];
            $paidTime = $this->changePaymentDate($customer->credit->payment_date)['time'];
        }

        $colorAgreementClass = $this->changeAgreementDate($customer->next_contract_date)['color'];
        $agreementTime = $this->changeAgreementDate($customer->next_contract_date)['agreement'];

        return view(
            'customerProcess.index',
            compact(
                'processes',
                'customer',
                'partners',
                'modePurchase',
                'fileValuation',
                'garage',
                'valuations',
                'purposes',
                'selectBanks',
                'tranche',
                'colorClass',
                'paidTime',
                'agreementTime',
                'colorAgreementClass',
                'newBanks',
                'message'
            )
        );
    }

    /**
     * Activity logs.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function logActivity()
    {
        $ses = [];
        $activity = [];
        $agent = new Agent();
        $sessions = Session::registered()->get();
        $logs = LogActivity::logActivityLists();

        foreach ($logs as $i => $log) {

            $activity[$i]['title'] = $log->subject;
            $activity[$i]['url'] = $log->url;
            $activity[$i]['method'] = $log->method;
            $activity[$i]['ip'] = $log->ip;

            $agent->setUserAgent($log->user_agent);

            $activity[$i]['platform'] = $agent->platform();
            $activity[$i]['browser'] = $agent->browser();

            $activity[$i]['name'] = User::findOrFail($log->user_id)->name;
        }

        foreach ($sessions as $i => $session) {

            $ses[$i]['name'] = User::findOrFail($session->user_id)->name;

            $agent->setUserAgent($session->user_agent);

            $ses[$i]['platform'] = $agent->platform();
            $ses[$i]['browser'] = $agent->browser();
            $ses[$i]['time'] = Carbon::createFromTimestamp($session->last_activity)->toDateTimeString();
        }

        return view('customerProcess.logActivity', compact('activity', 'ses'));
    }

    /**
     * Check if the user with the same login is editing the same page.
     *
     * @param $customerId
     * @return string
     */
    private function checkIfSameUserEditThisPage($customerId)
    {
        $message = '';
        $sessions = Session::registered()->get();
        $counter = count($sessions);

        if ($counter > 1) {

            foreach ($sessions as $user) {

                $logs = LogActivity::getLogByParam([
                    'user_id' => $user->user_id,
                    'ip' => $user->ip_address,
                    'url' => url('customer/process/' . $customerId)
                ]);
            }

            if (count($logs) > 1) {
                $message = 'Inny użytkownik jest już na tej stronie i może edytować dane aktualnego procesu. 
                    Opuścić stronę aby uniknąć problemów z utratą zapisanych informacji.';
            }
        }

        return $message;
    }

    /**
     * Start the browser for the detected operating system.
     *
     * @param $browserName
     * @param $url
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function runBrowser($browserName, $url)
    {
        $agent = new Agent();
        $platform = $agent->platform();
        $browser = $agent->browser();

        $browserName = escapeshellarg(escapeshellcmd($browserName));

        if ($browser != $browserName) {

            if ($platform == 'OS X') {

                exec('open -a ' . $browserName . ' ' . base64_decode($url));

            } elseif ($platform == 'Windows') {

                exec('start "' . $browserName . '" ' . base64_decode($url));

            } elseif ($platform == 'Linux' || $platform == 'Unix') {

                exec('xdg-open ' . $browserName . ' ' . base64_decode($url));

            } else {

                return redirect(base64_decode($url));
            }

        } else {

            return redirect(base64_decode($url));
        }

        return back();
    }

    /**
     * Save the file with people from the bank.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveContactFile(Request $request)
    {
        request()->validate([
            'file_contact' => 'file|max:20480'
        ]);

        if ($request->hasFile('file_contact')) {

            $name = $request->file('file_contact')->getClientOriginalName();

            Storage::put('public/banks/contact/' . $name, file_get_contents($request->file('file_contact')));

            Bank::findOrFail($request->bank_id)
                ->update(['contact_file' => $name]);

            $message = 'Plik kontaktu został dodany!';
        }

        $data = [
            'result' => $name,
            'message' => $message
        ];

        return response()->json($data);
    }

    /**
     * Change the date of the contract.
     *
     * @param $dataTime
     * @return array
     */
    public function changeAgreementDate($dataTime)
    {
        return [
            'color' => Customer::setTimeOutColor($dataTime),
            'agreement' => Customer::compareDate4Agreement($dataTime),
        ];
    }

    /**
     * Change the date of payment.
     *
     * @param $dataTime
     * @return array
     */
    public function changePaymentDate($dataTime)
    {
        return [
            'color' => Customer::setTimeOutColor($dataTime),
            'time' => Customer::compareDate4Payment($dataTime),
        ];
    }

    /**
     * Add new banks.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addNewBanks(Request $request)
    {
        $result = $message = '';

        try {

            $result = $this->setDefaultProcesses4Bank($request->customer_id, $request->id);
            $message = 'Bank został dodany!';

        } catch (\Exception $e) {
            $message = $e->getMessage();
        }

        $data = [
            'result' => $result,
            'message' => $message
        ];

        return response()->json($data);
    }

    /**
     * Set the default processes for the bank.
     *
     * @param $customerId
     * @param $bankId
     * @return array
     */
    private function setDefaultProcesses4Bank($customerId, $bankId)
    {
        $customer = Customer::findOrFail($customerId);
        $processes = Process::where('bank_id', $bankId)->get();

        $customerProcess = $customer->process()
            ->where('bank_id', $bankId)
            ->where('customer_id', $customer->id)
            ->first();

        $this->deleteEmptyProcessName($customer, $bankId);

        if (!$customerProcess) {
            foreach ($processes as $process) {

                $customer->process()->create([
                    'customer_id' => $customer->id,
                    'bank_id' => $process->bank_id,
                    'name' => $process->name,
                    'order' => $process->order
                ]);
            }
        } else {
            $customerProcess->where('bank_id', $bankId)
                ->where('customer_id', $customer->id)
                ->update(['status' => 1]);
        }

        return [
            'id' => $bankId,
            'name' => $processes[0]->bank->name,
            'url' => $processes[0]->bank->url,
            'logo' => $processes[0]->bank->logo
        ];
    }

    /**
     * Delete the empty process name.
     *
     * @param Customer $customer
     * @param $bankId
     */
    private function deleteEmptyProcessName(Customer $customer, $bankId)
    {
        $customer->process()->where('bank_id', $bankId)
            ->where('customer_id', $customer->id)
            ->whereNull('name')
            ->delete();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $customerId
     * @param $bankId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($customerId, $bankId)
    {
        $process = CustomerProcess::where('bank_id', $bankId)
            ->where('customer_id', $customerId)
            ->get();

        return view('customerProcess.edit', compact('process'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)
    {
        request()->validate([
            'name' => 'max:256',
        ]);

        $processes = CustomerProcess::where('bank_id', $request->bid)
            ->where('customer_id', $request->cid)
            ->where('status', 1)
            ->get();

        if ($request->process_id[0] || $request->process_name[0]) {

            foreach ($request->process_name as $order => $item) {

                if (!$request->process_id[$order]) {

                    CustomerProcess::create([
                        'bank_id' => $request->bid,
                        'customer_id' => $request->cid,
                        'name' => $item,
                        'order' => $order,
                        'bank_prefer' => $processes[0]->bank_prefer
                    ]);

                } else {

                    foreach ($processes as $process) {

                        if ($process->id === (int) $request->process_id[$order]) {

                            $process->update([
                                'name' => $item,
                                'order' => $order
                            ]);

                        }
                    }
                }
            }
        }

        return redirect('customer/process' . '/' . $request->cid)->with('status', 'Procesy zostały zakualizowane!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        CustomerProcess::find($id)
            ->delete();

        return back()->with('status', 'Proces został usunięty!');
    }

    /**
     * Remove the bank from the list.
     *
     * @param $customerId
     * @param $bankId
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteBankFromlist($customerId, $bankId)
    {
        CustomerProcess::where('bank_id', $bankId)
            ->where('customer_id', $customerId)
            ->delete();

        $data = [
            'message' => 'Bank wraz ze wszystkimi procesami został usunięty!'
        ];

        return response()->json($data);
    }

    /**
     * Delete the bank.
     *
     * @param $customerId
     * @param $bankId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteBank($customerId, $bankId)
    {
        CustomerProcess::where('bank_id', $bankId)
            ->where('customer_id', $customerId)
            ->delete();

        return back()->with('status', 'Bank wraz ze wszystkimi procesami został usunięty!');
    }

    /**
     * Set the status for the bank.
     *
     * @param $customerId
     * @param $bankId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function setStatus($customerId, $bankId)
    {
        CustomerProcess::where('bank_id', $bankId)
            ->where('customer_id', $customerId)
            ->update(['status' => 1]);

        return back()->with('status', 'Bank został przywrócony!');
    }

    /**
     * Unset the status for the bank.
     *
     * @param $customerId
     * @param $bankId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function unsetStatus($customerId, $bankId)
    {
        CustomerProcess::where('bank_id', $bankId)
            ->where('customer_id', $customerId)
            ->update(['status' => 0]);

        return back()->with('status', 'Bank został wyłączony!');
    }

    /**
     * Set the process for the bank as active.
     *
     * @param Request $request
     */
    public function activate(Request $request)
    {
        $customers = CustomerProcess::where('bank_id', $request->bid)
            ->where('customer_id', $request->cid)
            ->get();

        foreach ($customers as $customer) {
            if ($customer->id === (int) $request->id) {
                $customer->update(['active' => 1]);
            } else {
                $customer->update(['active' => 0]);
            }
        }
    }

    /**
     * Set the color.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setColor(Request $request)
    {
        $result = CustomerProcess::findOrFail($request->id)
            ->update(['color' => $request->color]);

        return response()->json($result);
    }

    /**
     * Set the date.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setDate(Request $request)
    {
        $date = date('Y-m-d H:i:s', strtotime($request->date));

        $result = CustomerProcess::findOrFail($request->id)
            ->update(['date' => $date]);

        return response()->json($result);
    }

    /**
     * Set your preferred bank.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setBankPrefer(Request $request)
    {
        $result = $message = '';

        try {

            $result = CustomerProcess::where('bank_id', $request->id)
                ->update(['bank_prefer' => $request->bank_prefer]);
            $message = 'Priorytet został aktualizowany!';

        } catch (\Exception $e) {
            $message = $e->getMessage();
        }

        $data = [
            'result' => $result,
            'message' => $message
        ];

        return response()->json($data);
    }
}
