<?php

namespace App\Http\Controllers;

use App\ModePurchase;
use Illuminate\Http\Request;

class ModePurchasesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modePurchases = ModePurchase::all();

        return view('configuration.modePurchases.index', compact('modePurchases'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('configuration.modePurchases.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required',
        ]);

        ModePurchase::create($request->all());

        return redirect('configuration/mode_purchases')->with('status', 'Tryb zakupu został dodany.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $modePurchase = ModePurchase::findOrFail($id);

        return view('configuration.modePurchases.edit', compact('modePurchase'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate([
            'name' => 'required',
        ]);

        ModePurchase::findOrFail($id)->update($request->all());

        return redirect('configuration/mode_purchases')->with('status', 'Tryb zakupu został zaktualizowany.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ModePurchase::find($id)->delete();

        return redirect('configuration/mode_purchases')->with('status', 'Tryb zakupu został usunięty.');
    }
}
