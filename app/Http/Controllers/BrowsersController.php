<?php

namespace App\Http\Controllers;

use App\Browser;
use Illuminate\Http\Request;

class BrowsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $browsers = Browser::all();

        return view('configuration.browsers.index', compact('browsers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('configuration.browsers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required',
            'path' => 'required',
            'os' => 'required',
        ]);

        Browser::create($request->all());

        return redirect('configuration/browsers')->with('status', 'Przeglądarka została dodana.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $browser = Browser::findOrFail($id);

        return view('configuration.browsers.edit', compact('browser'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate([
            'name' => 'required',
            'path' => 'required',
            'os' => 'required',
        ]);

        Browser::findOrFail($id)->update($request->all());

        return redirect('configuration/browsers')->with('status', 'Przeglądarka została zaktualizowana.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Browser::find($id)->delete();

        return redirect('configuration/browsers')->with('status', 'Przeglądarka została usunięta.');
    }
}
