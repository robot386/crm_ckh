<?php

namespace App\Http\Controllers;

use App\Role;
use App\RoleUser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();

        return view('configuration.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('name', 'id');
        return view('configuration.users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $roles = [];

        request()->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
        ]);

        if ($request->has('roles') && count($request->roles)) {
            foreach ($request->roles as $role) {
                $roles[] = $role;
            }
        } else {
            $roles[] = 3;
        }

        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ];

        User::create($data)
            ->roles()->attach($roles);

        return redirect('configuration/users')->with('status', 'Użytkownik został dodany.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //@TODO: profile user
        $user = User::findOrFail($id);

        return view('configuration.users.profile', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $roles = Role::pluck('name', 'id');

        return view('configuration.users.edit', compact('user', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $roles = [];
        $rules = [];
        $data = [];
        $user = User::findOrFail($id);

        if ($request->has('roles') && count($request->roles)) {
            foreach ($request->roles as $role) {
                $roles[] = $role;

                $user->roles()->detach();
            }
        } else {
            $roles[] = 3;
        }

        if ($request->has('name') && strlen($request->name) && $request->name !== $user->name) {

            $data['name'] = $request->name;
            $rules['name'] = 'required|string|max:255';
        }

        if ($request->has('email') && strlen($request->email) && $request->email !== $user->email) {

            $data['email'] = $request->email;
            $rules['email'] = 'required|string|email|max:255|unique:users';
        }

        if ($request->has('password') && strlen($request->password) && $request->password !== $user->password) {

            $data['password'] = $request->password;
            $rules['password'] = 'required|string|min:8|confirmed|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/';
        }

        request()->validate($rules);

        $user->update($data);

        $user->roles()->attach($roles);

        return redirect('configuration/users')->with('status', 'Użytkownik został zaktualizowany.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        $user->roles()->detach();

        $user->delete();

        return redirect('configuration/users')->with('status', 'Użytkownik został usunięty.');
    }
}
