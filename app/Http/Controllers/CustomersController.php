<?php

namespace App\Http\Controllers;

use App\Bank;
use App\Credit;
use App\Customer;
use App\CustomerProcess;
use App\FileValuation;
use App\Garage;
use App\Http\Requests\CustomerRequest;
use App\ModePurchase;
use App\Process;
use App\Property;
use App\Purchase;
use App\Purpose;
use App\Valuation;
use Illuminate\Support\Facades\Storage;

class CustomersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::where('active', 1)
            ->whereNull('customer_id')
            ->latest()
            ->get();

        return view('customers.index', compact('customers'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function archive()
    {
        $customers = Customer::where('active', 0)
            ->whereNull('customer_id')
            ->latest()
            ->get();

        return view('customers.index', compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $modePurchase = ModePurchase::pluck('name', 'id');
        $garage = Garage::pluck('name', 'id');
        $valuations = Valuation::pluck('name', 'id');
        $purposes = Purpose::pluck('name', 'id');
        $banks = Bank::pluck('name', 'id');

        return view(
            'customers.create',
            compact(
                'modePurchase',
                'garage',
                'valuations',
                'purposes',
                'banks',
                'newBanks'
            )
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CustomerRequest $request)
    {
        $result = $message = '';

        try {

            if (!$request->id) {

                $result = $this->saveCustomer($request);
                $message = 'Użytkownik został dodany!';
            } else {

                $result = $this->updateCustomer($request, $request->id);
                $message = 'Użytkownik został aktualizowany!';
            }

        } catch (\Exception $e) {
            $message = $e->getMessage();
        }

        $data = [
            'result' => $result,
            'message' => $message,
        ];

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CustomerRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(CustomerRequest $request, $id)
    {
        try {

            $result = $this->updateCustomer($request, $id);
            $message = 'Użytkownik został aktualizowany!';

        } catch (\Exception $e) {
            $message = $e->getMessage();
        }

        $data = [
            'result' => $result,
            'message' => $message
        ];

        return response()->json($data);
    }

    /**
     * Add a credit partner.
     *
     * @param CustomerRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addCreditPartner(CustomerRequest $request)
    {
        $result = $message = '';

        try {

            if (!$request->id) {
                $result = Customer::create($this->prepareData($request)['partner']);
                $message = 'Współkredytobiorca został dodany!';
            } else {
                $result = Customer::findOrFail($request->id)
                    ->update($this->prepareData($request)['partner']);
                $message = 'Współkredytobiorca został aktualizowany!';
            }

        } catch (\Exception $e) {
            $message = $e->getMessage();
        }

        $data = [
            'result' => $result,
            'message' => $message
        ];

        return response()->json($data);
    }

    /**
     * Delete the credit partner.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deletePartner($id)
    {
        Customer::find($id)->delete();

        $data = [
            'message' => 'Partner został usunięty!'
        ];

        return response()->json($data);
    }

    /**
     * Data update for the bank's customer.
     *
     * @param CustomerRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateOnBank(CustomerRequest $request, $id)
    {
        try {
            $result = $this->updateCustomerOnBank($request, $id);
            $message = 'Użytkownik został aktualizowany!';
        } catch (\Exception $e) {
            $message = $e->getMessage();
        }

        $data = [
            'result' => $result,
            'message' => $message
        ];

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        CustomerProcess::where('customer_id', $id)->delete();
        Property::where('customer_id', $id)->delete();
        Purchase::where('customer_id', $id)->delete();
        Credit::where('customer_id', $id)->delete();
        Customer::find($id)->delete();

        return redirect('customers/archive')->with('status', 'Użytkownik został usunięty!');
    }

    /**
     * Set the client as active.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function activate($id)
    {
        Customer::findOrFail($id)->update(['active' => 1]);

        return redirect('customers')->with('status', 'Użytkownik został przywrócony z archiwum!');
    }

    /**
     * Set the client to be inactive.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deactivate($id)
    {
        Customer::findOrFail($id)->update(['active' => 0]);

        return redirect('customers/archive')->with('status', 'Użytkownik został zarchiwizowany!');
    }

    /**
     * Delete the file.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function fileDelete($id)
    {
        $file = FileValuation::find($id);

        $path = 'public/users/' . $file->property->customer_id . '/wycena_nieruchomosci/' . $file->name;

        if ($file->delete()) {
            Storage::delete($path);
        }

        $response = [
            'message' => 'Plik został usunięty!'
        ];

        return response()->json($response);
    }

    /**
     * Save the customer.
     *
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    private function saveCustomer($request)
    {
        $result = [];

        $customer = Customer::create($this->prepareData($request)['customer']);

        $customerId = $customer->id;

        if ($customerId) {

            $customer->credit()->create($this->prepareData($request, $customerId)['credit']);

            $customer->purchase()->create($this->prepareData($request, $customerId)['purchase']);

            $customer->property()->create($this->prepareData($request, $customerId)['property']);

            $files = $this->saveValuationFile($request, $customerId, $customer->property->id);

            if ($request->banks[0]) {

                foreach ($request->banks as $bankId) {

                    $this->setDefaultProcesses4Bank($customer, $bankId);
                }
            }
        }

        $result = [
            'customer_id' => $customerId,
            'property_id' => $customer->property->id,
            'files' => $files
        ];

        return response()->json($result);
    }

    /**
     * Update the customer.
     *
     * @param $request
     * @param $customerId
     * @return \Illuminate\Http\JsonResponse
     */
    private function updateCustomer($request, $customerId)
    {
        $result = [];

        $customer = Customer::findOrFail($customerId);

        $customer->update($this->prepareData($request)['customer']);

        $customer->credit()->update($this->prepareData($request, $customerId)['credit']);

        $customer->purchase()->update($this->prepareData($request, $customerId)['purchase']);

        $customer->property()->update($this->prepareData($request, $customerId)['property']);

        $files = $this->saveValuationFile($request, $customerId, $customer->property->id);

        $customer->process()->update(['status' => 0]);

        if ($request->banks[0]) {

            foreach ($request->banks as $bankId) {

                $this->setDefaultProcesses4Bank($customer, $bankId);
            }
        }

        $result = [
            'customer_id' => $customerId,
            'property_id' => $customer->property->id,
            'files' => $files
        ];

        return response()->json($result);
    }

    /**
     * Save the valuation file.
     *
     * @param $request
     * @param $customerId
     * @param $propertyId
     * @return array
     */
    public function saveValuationFile($request, $customerId, $propertyId)
    {
        $result = [];

        if ($request->hasFile('file_valuation') && $propertyId) {

            foreach ($request->file_valuation as $i => $file) {

                $name = $file->getClientOriginalName();

                Storage::put('public/users/' . $customerId . '/wycena_nieruchomosci/' . $name, file_get_contents($file));

                $result[$i] = FileValuation::create([
                    'property_id' => $propertyId,
                    'name' => $name
                ]);
            }
        }

        return $result;
    }

    /**
     * Update the bank's customer.
     *
     * @param $request
     * @param $customerId
     * @return \Illuminate\Http\JsonResponse
     */
    private function updateCustomerOnBank($request, $customerId)
    {
        $result = [];

        $customer = Customer::findOrFail($customerId);

        $customer->update($this->prepareData($request)['customer']);

        $customer->credit()->update($this->prepareData($request, $customerId)['credit']);

        $customer->purchase()->update($this->prepareData($request, $customerId)['purchase']);

        $customer->property()->update($this->prepareData($request, $customerId)['property']);

        $files = $this->saveValuationFile($request, $customerId, $customer->property->id);

        $result = [
            'customer_id' => $customerId,
            'property_id' => $customer->property->id,
            'files' => $files
        ];

        return response()->json($result);
    }

    /**
     * Prepare the data.
     *
     * @param $request
     * @param null $customerId
     * @return mixed
     */
    private function prepareData($request, $customerId = null)
    {
        $result['partner'] = [
            'customer_id' => $request->customer_id ?? null,
            'name' => $request->name,
            'phone' => $request->phone,
            'email' => $request->email,
            'address' => $request->address,
            'active' => 1,
        ];

        $result['customer'] = [
            'name' => $request->name,
            'phone' => $request->phone,
            'email' => $request->email,
            'address' => $request->address,
            'active' => 1,
            'next_contract_date' => $request->next_contract_date
                ? date('Y-m-d H:i:s', strtotime($request->next_contract_date))
                : date('Y-m-d H:i:s'),
            'source_customer_description' => $request->source_customer_description,
        ];

        $result['credit'] = [
            'customer_id' => $customerId,
            'purpose_id' => $request->purpose_id,
            'amount' => $request->amount,
            'amount_own' => $request->amount_own,
            'description' => $request->description,
            'property_description' => $request->property_description,
            'payment_date' => $request->payment_date
                ? date('Y-m-d H:i:s', strtotime($request->payment_date))
                : date('Y-m-d H:i:s'),
            'loan_period' => $request->loan_period,
        ];

        $result['purchase'] = [
            'customer_id' => $customerId,
            'mode_purchase_id' => $request->mode_purchase_id,
            'price' => $request->price,
        ];

        $result['property'] = [
            'customer_id' => $customerId,
            'garage_id' => $request->garage_id,
            'valuation_id' => $request->valuation_id,
            'land_register' => $request->land_register,
            'plot_number' => $request->plot_number,
            'repairs_price' => $request->repairs_price,
        ];

        return $result;
    }

    /**
     * Set the bank's default processes.
     *
     * @param Customer $customer
     * @param $bankId
     */
    private function setDefaultProcesses4Bank(Customer $customer, $bankId)
    {
        $processes = Process::where('bank_id', $bankId)->get();

        $customerProcess = $customer->process()
            ->where('bank_id', $bankId)
            ->where('customer_id', $customer->id)
            ->first();

        $this->deleteEmptyProcessName($customer, $bankId);

        if (!$customerProcess) {
            foreach ($processes as $process) {

                $customer->process()->create([
                    'customer_id' => $customer->id,
                    'bank_id' => $process->bank_id,
                    'name' => $process->name,
                    'order' => $process->order
                ]);
            }
        } else {
            $customerProcess
                ->where('bank_id', $bankId)
                ->where('customer_id', $customer->id)
                ->update(['status' => 1]);
        }
    }

    /**
     * Delete the empty process name.
     *
     * @param Customer $customer
     * @param $bankId
     */
    private function deleteEmptyProcessName(Customer $customer, $bankId)
    {
        $customer->process()->where('bank_id', $bankId)
            ->where('customer_id', $customer->id)
            ->whereNull('name')
            ->delete();
    }
}
