<?php

namespace App\Http\Controllers;

use App\Bank;
use App\Browser;
use App\Process;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Jenssegers\Agent\Agent;

class BanksController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banks = Bank::orderBy('name')->get();

        return view('configuration.banks.index', compact('banks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $agent = new Agent();
        $platform = $agent->platform();

        $browsers = Browser::where('os', 'LIKE', "%$platform%")->pluck('name', 'id');

        return view('configuration.banks.create', compact('browsers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required',
            'url' => 'required',
            'logo' => 'image|mimes:jpeg,jpg,png,gif,svg|max:2048',
            'contact_file' => 'file|max:20480'
        ]);

        $data = [
            'name' => $request->name,
            'url' => $request->url,
            'browser_id' => $request->browser_id
        ];

        if ($request->hasFile('logo')) {

            $data['logo'] = $this->normalizeName($request->logo->getClientOriginalName());
            $request->logo->move(storage_path('app/public/banks/logo'), $data['logo']);
        }

        if ($request->hasFile('contact_file')) {

            $data['contact_file'] = $this->normalizeName($request->contact_file->getClientOriginalName());
            $request->contact_file->move(storage_path('app/public/banks/contact'), $data['contact_file']);
        }

        $bank = Bank::create($data);

        if (!$request->process[0] && $bank->id) {
            Process::create([
                'bank_id' => $bank->id,
                'name' => 'Złożenie wniosku',
                'order' => 0,
            ]);
        } elseif ($request->process[0] != null && $bank->id) {
            foreach ($request->process as $i => $name) {
                Process::create([
                    'bank_id' => $bank->id,
                    'name' => $name,
                    'order' => $i,
                ]);
            }
        } else {
            return back()->with('errors', 'Uzupełnij dane.');
        }

        return redirect('configuration/banks')->with('status', 'Bank został dodany.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $agent = new Agent();
        $platform = $agent->platform();

        $bank = Bank::findOrFail($id);
        $browsers = Browser::where('os', 'LIKE', "%$platform%")->pluck('name', 'id');

        return view('configuration.banks.edit', compact('bank', 'browsers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate([
            'name' => 'required',
            'url' => 'required',
            'logo' => 'image|mimes:jpeg,jpg,png,gif,svg|max:2048',
            'contact_file' => 'file|max:20480'
        ]);

        $bank = Bank::findOrFail($id);

        $data = [
            'name' => $request->name,
            'url' => $request->url,
            'browser_id' => $request->browser_id
        ];

        if ($request->hasFile('logo')) {

            if ($bank->logo) {

                $path = storage_path('app/public/banks/logo') . '/' . $bank->logo;
                Storage::delete($path);
            }

            $data['logo'] = $this->normalizeName($request->logo->getClientOriginalName());
            $request->logo->move(storage_path('app/public/banks/logo'), $data['logo']);
        }

        if ($request->hasFile('contact_file')) {

            if ($bank->contact_file) {

                $path = storage_path('app/public/banks/contact') . '/' . $bank->contact_file;
                Storage::delete($path);
            }

            $data['contact_file'] = $this->normalizeName($request->contact_file->getClientOriginalName());
            $request->contact_file->move(storage_path('app/public/banks/contact'), $data['contact_file']);
        }

        $bank->update($data);

        if (!$request->process[0]) {

            $this->deleteProcessBank($id);
            
            Process::create([
                'bank_id' => $bank->id,
                'name' => 'Złożenie wniosku',
                'order' => 0,
            ]);
        } elseif ($request->process[0] != null) {

            $this->deleteProcessBank($id);

            foreach ($request->process as $i => $name) {
                Process::create([
                    'bank_id' => $bank->id,
                    'name' => $name,
                    'order' => $i,
                ]);
            }
        } else {
            return back()->with('errors', 'Uzupełnij dane.');
        }

        return redirect('configuration/banks')->with('status', 'Bank został zaktualizowany.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->deleteProcessBank($id);

        Bank::find($id)->delete();

        return redirect('configuration/banks')->with('status', 'Bank został usunięty.');
    }

    /**
     * Removing bank processes.
     *
     * @param $id
     */
    private function deleteProcessBank($id)
    {
        Process::where('bank_id', $id)->delete();
    }

    /**
     * Normalizes the name.
     *
     * @param $string
     * @param string $separator
     * @return string
     */
    private function normalizeName($string, $separator = '_')
    {
        $title = '';

        $array = explode('.', $string);

        $extension = $array[count($array)-1];

        array_pop($array);

        foreach ($array as $str) {
            $title .= $str;
        }

        $title = Str::ascii($title);

        // Convert all dashes/underscores into separator
        $flip = $separator == '-' ? '_' : '-';

        $title = preg_replace('![' . preg_quote($flip) . ']+!u', $separator, $title);

        // Replace @ with the word 'at'
        $title = str_replace('@', $separator . 'at' . $separator, $title);

        // Remove all characters that are not the separator, letters, numbers, or whitespace.
        $title = preg_replace('![^' . preg_quote($separator) . '\pL\pN\s]+!u', '', $title);

        // Replace all separator characters and whitespace by a single separator
        $title = preg_replace('![' . preg_quote($separator) . '\s]+!u', $separator, $title);

        return trim($title, $separator) . '.' . $extension;
    }
}
