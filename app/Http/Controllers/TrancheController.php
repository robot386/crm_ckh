<?php

namespace App\Http\Controllers;

use App\Tranche;
use Illuminate\Http\Request;

class TrancheController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'description' => 'required',
        ]);

        if (!$request->id) {

            $result = Tranche::create($request->all());
            $message = 'Dodano arkusz.';
        } else {

            $result = Tranche::findOrFail($request->id)
                ->update($request->all());
            $message = 'Aktualizowano arkusz.';
        }

        $data = [
            'result' => $result,
            'message' => $message
        ];

        return response()->json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Tranche::findOrFail($id);

        return response()->json($data->description);
    }
}
