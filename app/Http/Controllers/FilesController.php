<?php

namespace App\Http\Controllers;

use App\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FilesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('files.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $data = [];

        if ($request->hasFile('file') && $request->customer_process_id && $request->customer_id) {

            $name = $request->file->getClientOriginalName();
            $path = 'users/' . $request->customer_id . '/' . $request->bank_id . '/' . $request->customer_process_id . '/' . $name;

            Storage::put('public/' . $path, file_get_contents($request->file));

            $result = File::create([
                'customer_process_id' => $request->customer_process_id,
                'name' => $name,
            ]);
            $message = 'Zapisano plik.';

            $data = [
                'result' => $result,
                'message' => $message,
                'path' => 'storage/' . $path
            ];
        }

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $file = File::find($id);

        $path = 'public/users/' . $file->process->customer_id . '/' . $file->process->bank_id . '/' . $file->customer_process_id . '/' . $file->name;

        if ($file->delete()) {
            Storage::delete($path);
        }

        $response = [
            'message' => 'Plik został usunięty!'
        ];

        return response()->json($response);
    }
}
