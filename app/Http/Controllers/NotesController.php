<?php

namespace App\Http\Controllers;

use App\Note;
use Illuminate\Http\Request;

class NotesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Note::where('customer_process_id', $request->customer_process_id)->update(['active' => 0]);

        request()->validate([
            'description' => 'required',
        ]);

        if (!$request->id) {

            $result = Note::create($request->all());
            $message = 'Dodano notatkę.';
        } else {

            $note = Note::findOrFail($request->id);
            $note->update($request->all());
            $result = $note;
            $message = 'Aktualizowano notatkę.';
        }

        $data = [
            'result' => $result,
            'message' => $message
        ];

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Note::find($id)->delete();

        $data = [
            'message' => 'Usunięto notatkę.'
        ];

        return response()->json($data);
    }
}
