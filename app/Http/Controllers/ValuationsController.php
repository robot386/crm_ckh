<?php

namespace App\Http\Controllers;

use App\Valuation;
use Illuminate\Http\Request;

class ValuationsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $valuations = Valuation::all();

        return view('configuration.valuations.index', compact('valuations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('configuration.valuations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required',
        ]);

        Valuation::create($request->all());

        return redirect('configuration/valuations')->with('status', 'Wycena została dodana.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $valuation = Valuation::findOrFail($id);

        return view('configuration.valuations.edit', compact('valuation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate([
            'name' => 'required',
        ]);

        Valuation::findOrFail($id)->update($request->all());

        return redirect('configuration/valuations')->with('status', 'Wycena została zaktualizowana.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Valuation::find($id)->delete();

        return redirect('configuration/valuations')->with('status', 'Wycena została usunięta.');
    }
}
