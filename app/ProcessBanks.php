<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProcessBanks extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'bank_id',
        'process_id',
    ];
}
