<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'browser_id',
        'name',
        'logo',
        'url',
        'contact_file',
    ];

    /**
     * Get all of the customer processes for the bank.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function customerProcess()
    {
        return $this->hasMany(CustomerProcess::class);
    }

    /**
     * Get all of the processes for the bank.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function process()
    {
        return $this->hasMany(Process::class);
    }

    /**
     * Get the browser for the bank.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function browser()
    {
        return $this->belongsTo(Browser::class);
    }

    /**
     * Get all of the tranches for the bank.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tranche()
    {
        return $this->hasMany(Tranche::class);
    }

    /**
     * Get only new selected the bank.
     *
     * @param $banks
     * @param $selectBanks
     * @return array
     */
    public static function selectNewBank($banks, $selectBanks)
    {
        $oldBanks = $newBanks = [];

        if ($selectBanks) {
            foreach ($banks as $i => $bank) {

                $ids = array_unique($selectBanks);

                foreach ($ids as $id) {
                    if ($i == $id) {
                        $oldBanks[$i] = $bank;
                    } else {
                        $newBanks[$i] = $bank;
                    }
                }
                $newBanks = array_diff($newBanks, $oldBanks);
            }
        } else {
            $newBanks = $banks;
        }

        return $newBanks;
    }
}
