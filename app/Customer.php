<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'customer_id',
        'name',
        'phone',
        'email',
        'address',
        'active',
        'next_contract_date',
        'source_customer_description',
    ];

    /**
     * Get the user for the customer.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the credit record associated with the customer.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function credit()
    {
        return $this->hasOne(Credit::class);
    }

    /**
     * Get all of the processes for the customer.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function process()
    {
        return $this->hasMany(CustomerProcess::class);
    }

    /**
     * Get all of the tranches for the customer.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tranche()
    {
        return $this->hasMany(Tranche::class);
    }

    /**
     * Get the purchase record associated with the customer.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function purchase()
    {
        return $this->hasOne(Purchase::class);
    }

    /**
     * Get the property record associated with the customer.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function property()
    {
        return $this->hasOne(Property::class);
    }

    /**
     * Get the last process by bank ID.
     *
     * @param $bankId
     * @param int $active
     * @return mixed
     */
    public function getLastProcessByBankId($bankId, $active = 0)
    {
        return $this->process
            ->where('bank_id', $bankId)
            ->where('active', $active)
            ->first();
    }

    /**
     * Sort by preferences.
     *
     * @param $processes
     * @return array
     */
    public function sortByPrefer($processes)
    {
        $notpef = $prefer = [];

        foreach ($processes as $i => $process) {

            if ($process->bank_prefer) {
                $prefer[$process->bank_prefer][$process->bank_id] = $process->bank_id;
            } else {
                $notpef[0][$process->bank_id] = $process->bank_id;
            }
        }

        ksort($prefer);
        return array_merge($prefer, $notpef);
    }

    /**
     * Get distinct processes.
     *
     * @param $processes
     * @param int $active
     * @return array
     */
    public function getDistinctProcesses($processes, $active = 0)
    {
        $result = $bankIds = [];

        $processes = $this->sortByPrefer($processes);

        foreach ($processes as $process) {
            foreach ($process as $item) {
                $bankIds[] = $item;
            }
        }

        foreach ($bankIds as $id) {
            $result[] = $this->getLastProcessByBankId($id, $active);
        }

        return array_filter($result);
    }

    /**
     * Compare the date of the contract.
     *
     * @param $dataTime
     * @return string
     */
    public static function compareDate4Agreement($dataTime)
    {
        $string = '';

        Carbon::setLocale('pl');

        $now  = Carbon::now();
        $end  = Carbon::parse($dataTime);

        $day = $now->diffInDays($end, false);

        if ($day > 0) {
            $string = 'Kolejna umowa za ' . $day . ($day == 1 ? ' dzień.' : ' dni.');
        } elseif ($day == 0) {
            $string = 'Dzisiaj upływa termin umowy.';
        } elseif ($day < 0 && $day > -14599) {
            $string = 'Od ostatniej umowy upłynęło  ' . abs($day) . (abs($day) == 1 ? ' dzień.' : ' dni.');
        } elseif ($day < -14600) { // 40 lat
            $string = 'Nieprawidłowa data.';
        }

        return $string;
    }

    /**
     * Compare the date of payment.
     *
     * @param $dataTime
     * @return string
     */
    public static function compareDate4Payment($dataTime)
    {
        $string = '';

        Carbon::setLocale('pl');

        $now  = Carbon::now();
        $end  = Carbon::parse($dataTime);

        $day = $now->diffInDays($end, false);

        if ($day > 0) {
            $string = 'Do wypłaty pozostało ' . $day . ' dni.';
        } elseif ($day < -14600) { // 40 lat
            $string = 'Nieprawidłowa data.';
        } else {
            $string = 'Wypłacono ' . abs($day) . ' dni temu.';
        }

        return $string;
    }

    /**
     * Set the color for the timeout.
     *
     * @param $dataTime
     * @return string
     */
    public static function setTimeOutColor($dataTime)
    {
        $color = '';

        Carbon::setLocale('pl');

        $now  = Carbon::now();
        $end  = Carbon::parse($dataTime);

        $day = $now->diffInDays($end, false);

        if ($day <= 7) {
            $color = 'text-danger';
        } elseif ($day <= 14) {
            $color = 'text-primary';
        } elseif ($day < 0) {
            $color = 'text-success';
        }

        return $color;
    }
}
