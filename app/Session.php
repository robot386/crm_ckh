<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['payload'];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table = 'sessions';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get the user for the session.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Returns all the guest users.
     *
     * @return mixed
     */
    public function scopeGuests()
    {
        return $this->whereNull('user_id');
    }

    /**
     * Returns all the registered users.
     *
     * @return mixed
     */
    public function scopeRegistered()
    {
        return $this->whereNotNull('user_id')->with('user');
    }

    /**
     * Updates the session of the current user.
     *
     * @return mixed
     */
    public function scopeUpdateCurrent()
    {
        $this->closeTimeOutSession();

        $userId = (auth()->check()) ? auth()->user()->id : null;

        return $this->where('id', session()->getId())
            ->update([
                'user_id' => $userId
            ]);
    }

    /**
     * Close the session after a time limit.
     */
    private function closeTimeOutSession()
    {
        $date = Carbon::now()->subMinutes(config('session.lifetime'))->timestamp;

        $this->where('last_activity', '<=' , $date)->delete();
    }

    /**
     * Status color switch.
     *
     * @return array
     */
    public function scopeSwitchStatus()
    {
        $result = [];
        $colorUser = 'success';
        $colorGuest = 'secondary';
        $countUsers = $this->registered()->count();
        $countGuests = $this->guests()->count();

        switch ($countUsers) {
            case 2:
                $colorUser = 'warning';
                break;
            case 3:
                $colorUser = 'danger';
                break;
            case $countUsers > 3:
                $colorUser = 'dark';
                break;
        }

        switch ($countGuests) {
            case 2:
                $colorGuest = 'info';
                break;
            case 3:
                $colorGuest = 'primary';
                break;
            case $countGuests > 3:
                $colorGuest = 'dark';
                break;
        }

        $result = [
            'users' => [
                'count' => $countUsers,
                'color' => $colorUser
            ],
            'guests' => [
                'count' => $countGuests,
                'color' => $colorGuest
            ]
        ];

        return $result;
    }
}
