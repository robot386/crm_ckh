$(document).ready(function() {

    /**
     * Replace datepicker
     */

    $('.datepicker').datepicker({
        format: 'dd-mm-yyyy',
        weekStart: 1,
        autoclose: true,
        language: 'pl'
    });


    /**
     * Url for customer process
     */

    if (routeProcess) {

        if (url.indexOf('#') > -1) {
            var arr = url.split('#');
            url = arr[0];
        }

        document.location = url + '#activeProcess';
    }

    /**
     * Change name
     */

    $('input[type="text"].name-customer').change(function () {
        $('.current-name').text($(this).val());
    });

    $('input[type="text"].name-partner').change(function () {
        var id = $(this).data('partner_id');
        $('.partner-name-' + id).text($(this).val());
    });


    /**
     * Add new bank
     */

    $('ul#selectNewBank > li > input[type=checkbox]').click(function () {

        var actionForm = $('#addNewBankForm').attr('action');
        var customer_id = $('.customer_id').val();
        var self = $(this);
        var id = $(self).val();

        if ($(self).is(':checked')) {
            $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
            $.ajax({
                type:'POST',
                url: actionForm,
                data: {id: id, customer_id: customer_id},
                dataType: 'json',
                success: function (data) {

                    if (data.result.id && data.result.name) {

                        $(self).parent().hide();

                        if (id !== undefined) {
                            $('#selectBankList').append(
                                '<tr class="row-' + data.result.id + '"><td><div class="form-flat">' +
                                '<input class="form-ghost" name="name" type="text" value="' + data.result.name + '">' +
                                '</div></td><td><div class="form-flat">' +
                                '<input class="form-ghost" data-prefer="' + data.result.id + '" name="bank_prefer" type="number">' +
                                '</div></td><td style="width: 1rem;"><button data-id="' + data.result.id +
                                '" data-customer_id="' + customer_id + '" type="button" ' +
                                'class="btn-flat-black pull-right bank-delete"><i class="fa fa-trash" aria-hidden="true">' +
                                '</i></button></td></tr>'
                            );
                        }

                        var block_content = '<div class="element-carousel container-carousel-' + data.result.id + '">' +
                            '<h5 class="pull-left"><span class="bank-prefer-' + data.result.id + '"></span>\n' +
                            '<a href="' + data.result.url + '" target="_blank" title="' + data.result.name + '">\n' +
                            '<img class="logo-size" src="../../storage/banks/logo/' + data.result.logo + '" alt="' + data.result.name + '"></a>\n' +
                            '<i class="fa fa-phone m-2" title="Brak pliku z danymi kontaktowymi" style="color: silver;"></i>\n' +
                            '</h5><div class="form-group text-right"><a href="#activeProcess" class="btn btn-outline-primary refresh-view">Odśwież widok</a>' +
                            '</div></div>';

                        $('.load-carousel').append(block_content);

                    }
                },
                error: function (xhr, desc, err) {

                    displayErrors(xhr, desc, err);
                }
            });
        }
    });

    /**
     * Refresh view
     */

    $('.load-carousel').on('click', '.refresh-view', function () {
        location.reload();
    });

    /**
     * Delete bank
     */

    $('tbody#selectBankList').on('click','.bank-delete', function() {
        var bank_id = $(this).data('id');
        var customer_id = $(this).data('customer_id');
        var time = 500;

        if (confirmDelete('ten bank')) {
            $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
            $.ajax({
                type: "POST",
                url: actionDeleteBank + '/' + customer_id + '/' + bank_id,
                data: {customer_id: customer_id, bank_id: bank_id, _method: 'delete'},
                success: function (data) {

                    if (communicate === true) {
                        $('.alert-process').html('<div class="alert alert-success" role="alert">' +
                            data.message + '</div>').fadeIn().fadeOut(3000);
                    }

                    $('tbody#selectBankList > .row-' + bank_id).fadeOut(time);
                    $('.container-carousel-' + bank_id).fadeOut(time);
                },
                error: function (xhr, desc, err) {

                    displayErrors(xhr, desc, err);
                }
            });
        }
    });

    /**
     * Change agreement date
     */

    $('#next_contract_date').change(function () {

        var date = $(this).val();

        $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
        $.ajax({
            type:'POST',
            url: actionChangeAgreementDate + '/' + date,
            data: {date: date},
            dataType: 'json',
            success: function (data) {

                $('.agreement-time').html('<b>' + data.agreement + '</b>');
                $('p.agreement-time').addClass(data.color);
            },
            error: function (xhr, desc, err) {

                displayErrors(xhr, desc, err);
            }
        });
    });

    /**
    * Change payment date
    */

    $('#payment_date').change(function () {

        var date = $(this).val();

        $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
        $.ajax({
            type:'POST',
            url: actionChangePaymentDate + '/' + date,
            data: {date: date},
            dataType: 'json',
            success: function (data) {

                $('.paid-time').html('<b>' + data.time + '</b>');
                $('p.paid-time').addClass(data.color);
            },
            error: function (xhr, desc, err) {

                displayErrors(xhr, desc, err);
            }
        });
    });

    /**
     * Add Files for new customer
     */

    function getActionForm(id) {
        return $(id).attr("action");
    }

    function getInputsForm(id, data) {
        $(id + " :input").each(function (i, item) {
            if (item.value.search('zł') > -1 || item.value.search(',00') > -1) {
                data.append(item.name, repearPrice(item.value));
            } else {
                data.append(item.name, item.value);
            }
        });
        return data;
    }

    function getInputsFormToArray(id, data) {
        $(id + " :input").each(function (i, item) {
            if (item.value.search('zł') > -1 || item.value.search(',00') > -1) {
                data[item.name] = repearPrice(item.value);
            } else {
                data[item.name] = item.value;
            }
        });
        return data;
    }

    var customer_id = $('.customer_id');
    var property_id = $('.property_id');

    if (customer_id && property_id) {
        $('#uploadFilesCustomer').on("change", function(e) {

            e.preventDefault();
            var self = $(this);
            var elementId = null;

            if ($("#createCustomerForm").length === 0) {
                elementId = "#updateCustomerForm";
            } else {
                elementId = "#createCustomerForm";
            }

            var actionForm = getActionForm(elementId);

            var file_valuation = $(self).prop('files');
            var form_data = new FormData();

            $(file_valuation).each(function (i, item) {
                form_data.append('file_valuation[' + i + ']', item);
            });

            form_data = getInputsForm(elementId, form_data);

            $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
            $.ajax({
                xhr: function() {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener('progress', function(e) {
                        if (e.lengthComputable) {
                            var percent = Math.round((e.loaded / e.total) * 100);
                            $('.progress').show();
                            $('#progressBar').attr('aria-valuenow', percent).css('width', percent + '%').text(percent + '%');
                        }
                    });
                    return xhr;
                },
                url: actionForm, // point to server-side PHP script
                data: form_data,
                type: 'POST',
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                processData: false,
                success: function(data) {

                    var res = [];

                    if (data.result.original) {

                        res = data.result.original.files;

                        $(res).each(function (i, item) {

                            $('#addNewFile').append('<tr class="row-' + item.id + '"><td>\n' +
                                '<a href="../storage/users/' + data.result.original.customer_id + '/wycena_nieruchomosci/' +
                                item.name + '" target="_blank">\n' + item.name +
                                '</a></td><td><a class="btn-flat-black pull-right delete-file" data-fileid="' +
                                item.id + '"><i class="fa fa-trash" aria-hidden="true"></i></a></td></tr>');
                        });

                        alert('Pliki zostały zapisane.');

                        $('#progressBar').attr('aria-valuenow', 0).css('width', 0 + '%').text(0 + '%');
                        $('.progress').hide();

                    } else {
                        alert('Pliki nie zostały zapisane.');
                    }
                }
            });
        });
    }

    /**
     * Add file contact
     */

    $('.file_contact_form').on("change", function(e) {

        e.preventDefault();

        var self = $(this);
        var id = $(self).attr('id');
        var actionForm = $('#' + id).attr("action");

        var file_contact = $(self).find('#file_contact').prop('files')[0];
        var bank_id = $(self).find('#bank_id').val();

        var form_data = new FormData();
        form_data.append('file_contact', file_contact);
        form_data.append('bank_id', bank_id);

        $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
        $.ajax({
            url: actionForm, // point to server-side PHP script
            data: form_data,
            type: 'POST',
            contentType: false, // The content type used when sending data to the server.
            cache: false, // To unable request pages to be cached
            processData: false,
            success: function(data) {

                $('.file_contact_link-' + bank_id).html(
                    '<a target="_blank" href="../../storage/banks/contact/' + data.result + '">' +
                    '<i class="fa fa-phone m-2"></i></a>'
                );
            }
        });
    });


    /**
     * Create customer
     */

    var CustomerId = $('form#createCustomerForm').find('.customer_id').val();
    
    if (CustomerId && CustomerId !== undefined) {
        $('.table-file').hide();
    }

    $("#createCustomerForm").change(function (e) {

        e.preventDefault();
        var communicate = false;
        var self = $(this);
        var actionForm = $(self).attr("action");
        var id = $(self).find('.customer_id').val();
        var data = {};

        data = getInputsFormToArray('#createCustomerForm', data);

        if (data.name.length > 0 && data.email.length > 0) {
            $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
            $.ajax({
                type:'POST',
                url: actionForm,
                data: data,
                dataType: 'json',
                success: function (data) {

                    // console.log(data);

                    var res = data.result.original;

                    if (res.customer_id) {
                        $('.customer_id').val(res.customer_id);
                        $('.property_id').val(res.property_id);
                        $('.table-file').show();
                    } else {
                        $('.customer_id').val(id);
                        $('.table-file').show();
                    }

                    if (communicate === true) {
                        $('.alert-process').html('<div class="alert alert-success" role="alert">' +
                            data.message + '</div>').fadeIn().fadeOut(3000);
                    }
                },
                error: function (xhr, desc, err) {

                    displayErrors(xhr, desc, err);
                }
            });
        }
    });


    /**
     * Update customer
     */

    $("#updateCustomerForm").change(function (e) {

        e.preventDefault();
        var communicate = false;
        var self = $(this);
        var actionForm = $(self).attr("action");
        var data = {};

        data = getInputsFormToArray('#updateCustomerForm', data);

        $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
        $.ajax({
            type:'PATCH',
            url: actionForm,
            data: data,
            dataType: 'json',
            success: function (data) {

                // console.log(data);

                if (communicate === true) {
                    $('.alert-process').html('<div class="alert alert-success" role="alert">' +
                        data.message + '</div>').fadeIn().fadeOut(3000);
                }
            },
            error: function (xhr, desc, err) {

                displayErrors(xhr, desc, err);
            }
        });
    });


    /**
     * Refresh bank list
     */

    $("#selectBankList").on('change', 'input[name="bank_prefer"]', function(e) {

        e.preventDefault();
        var self = $(e.target);
        var id = $(self).data('prefer');
        var value = $(self).val();

        $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
        $.ajax({
            type:'POST',
            url: actionSetBankPrefer,
            data: {id: id, bank_prefer: value},
            dataType: 'json',
            success: function (data) {

                if (communicate === true) {
                    $('.alert-process').html('<div class="alert alert-success" role="alert">' +
                        data.message + '</div>').fadeIn().fadeOut(3000);
                }

                $('.bank-prefer-' + id).html(value + ' - ');
            },
            error: function (xhr, desc, err) {

                displayErrors(xhr, desc, err);
            }
        });
    });


    /**
     * Delete customer file
     */

    $("#addNewFile").on('click','.delete-file', function() {
        var id = $(this).data('fileid');

        if (!id) {
            $('.row-').fadeOut(300);
        } else {
            deleteRecord(id, actionDeleteFile4Customer, communicate, '#addNewFile > .row-', 'ten plik');
        }
    });


    /**
     * Sortable
     */

    $("#table-sortable").sortable();


    /**
     * Toggle header
     */

    elementToggle('.head-toggle-tranche', '.element-toggle-tranche');
    elementToggle('.head-toggle-customer', '.element-toggle-customer');
    elementToggle('.head-toggle-partner', '.element-toggle-partner');
    elementToggle('.head-toggle-bank', '.element-toggle-bank');

    function elementToggle(start, react)
    {
        $(start).each(function () {
            var self = $(this);
            var element = $(react);

            element.hide();

            self.click(function () {
                element.toggle();
            });
        });
    }


    /**
     * Add credit partner
     */

    $('#addCreditPartner').click(function () {
        $('.credit-partner').append(
            '<div class="row-"><form method="POST" action="' + action4Partner + '" accept-charset="UTF-8" id="addCreditPartnerForm-">\n' +
            '<input name="customer_id" type="hidden" value="' + customerId + '"><input id="partnerId-" name="id" type="hidden" value="">\n' +
            '<table class="table table-bordered table-hover table-flat table-striped table-toggle" cellspacing="0" width="100%">\n' +
            '<thead><tr class="thead-dark"><th class="partner-name-' + partnerId + '">Nowy współkredytobiorca</th>' +
            '<th><a data-partner="' + partnerId + '" class="btn-flat pull-right del-partner"><i class="fa fa-remove"></i></a></th>' +
            '</tr></thead><tbody><tr><th class="table-secondary" scope="row">\n' +
            '<label for="name">Nazwisko i imię:</label><span class="text-danger pull-right">*</span></th><td><div class="form-flat">\n' +
            '<input class="form-ghost name-partner" name="name" type="text" value="" id="name"></div></td></tr><tr>\n' +
            '<th class="table-secondary" scope="row"><label for="phone">Telefon:</label></th><td><div class="form-flat">\n' +
            '<input class="form-ghost" name="phone" type="text" value="" id="phone"></div></td></tr><tr>\n' +
            '<th class="table-secondary" scope="row"><label for="email">E-mail:</label><span class="text-danger pull-right">*</span>' +
            '</th><td><div class="form-flat">\n' +
            '<input class="form-ghost" name="email" type="text" value="" id="email"></div></td></tr><tr>\n' +
            '<th class="table-secondary" scope="row"><label for="address">Adres:</label></th><td><div class="form-flat">\n' +
            '<input class="form-ghost" name="address" type="text" value="" id="address"></div></td></tr></tbody></table></form></div>'
        );
    });


    /**
     * Delete credit partner
     */

    $("#tableCreditPartner").on('click','.del-partner', function(e) {

        var id = $(this).data('partner');
        var element = $(e.target).parent().parent().parent().parent().parent();

        if (!id) {
            $(element).fadeOut(300);
        } else {
            deleteRecord(id, delete4Partner, communicate, '#tableCreditPartner li div.row-', 'tego partnera');
        }
    });


    /**
     * Save credit partner
     */

    $("ul#tableCreditPartner").change(function (e) {

        e.preventDefault();
        var self = $(this);
        var idForm = $(e.target).parent().parent().parent().parent().parent().parent().attr('id');
        var actionForm = $('#' + idForm).attr("action");
        var id = $('input[name=id]').val();
        var data = {};

        $('.partner-name-0').text($('.name-partner').val());

        $('#' + idForm + ' :input').each(function (i, item) {
            data[item.name] = item.value;
        });

        if (!id) {
            createCreditPartner(data, actionForm);
        } else {
            updateCreditPartner(data, actionForm);
        }
    });

    function createCreditPartner(data, actionForm)
    {
        var parentElement = $('.row-');
        var partner = $('#partnerId-');

        if (data.name.length > 0 && data.email.length > 0) {
            $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
            $.ajax({
                type: 'POST',
                url: actionForm,
                data: data,
                dataType: 'json',
                success: function (data) {

                    if (data && data.result.id) {
                        $(partner).val(data.result.id);
                        $(partner).attr('id', 'partnerId-' + data.result.id);
                        $(parentElement).attr('id', 'partnerCredit-' + data.result.id);
                        $(parentElement).attr('class', 'row-' + data.result.id);
                        $('#addCreditPartnerForm-').attr('id', 'addCreditPartnerForm-' + data.result.id);
                        $('.del-partner').attr('data-partner', data.result.id);
                    }

                    if (communicate === true) {
                        $('.alert-process').html('<div class="alert alert-success" role="alert">' +
                            data.message + '</div>').fadeIn().fadeOut(3000);
                    }
                },
                error: function (xhr, desc, err) {

                    displayErrors(xhr, desc, err);
                }
            });
        }
    }

    function updateCreditPartner(data, actionForm)
    {
        $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
        $.ajax({
            type:'POST',
            url: actionForm,
            data: data,
            dataType: 'json',
            success: function (data) {

                if (communicate === true) {
                    $('.alert-process').html('<div class="alert alert-success" role="alert">' +
                        data.message + '</div>').fadeIn().fadeOut(3000);
                }
            },
            error: function (xhr, desc, err) {

                displayErrors(xhr, desc, err);
            }
        });
    }

    function repearPrice(str)
    {
        return str.replace(',', '.').replace(/\s+/g, '').replace(/zł/g, '');
    }


    /**
     * Data save
     */

    $('.form-date').change(function () {

        var self = $(this);
        var id = $(self).data('id');
        var date = $(self).val();

        $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
        $.ajax({
            type: "POST",
            url: actionSaveDate,
            data: {id: id, date: date},
            success: function (data) {},
            error: function (xhr, desc, err) {

                displayErrors(xhr, desc, err);
            }
        });
    });


    /**
     * Color process
     */

    $(".color-process").spectrum({
        preferredFormat: "hex",
        showPaletteOnly: true,
        allowEmpty: true,
        palette: [
            ["#c00","#e69138","#f1c232","#6aa84f","#45818e","#3d85c6","#674ea7","#a64d79", "silver"]
        ],
        change: function(color) {

            var self = $(this);
            var id = $(self).data('id');

            if (id && color) {
                var newColor = color.toHexString();
                saveColorProcess(id, newColor);
            }
        }
    });


    /**
     * Bank view
     */

    $('.owl-carousel').owlCarousel({
        nav: true,
        navText: [ '<', '>' ],
        center: true,
        loop: false,
        margin: 10,
        dots: false,
        URLhashListener: true,
        autoplayHoverPause: true,
        startPosition: 'URLHash',
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:7
            }
        }
    });


    /**
     * Customer process
     */

    $(".add-process-customer").click(function() {
        $("#formCustomerProcess").append('<tr class="ui-sortable-handle"><td class="move"><i class="fa fa-arrows"></i></td>\n' +
            '<td><div class="form-flat"><input name="process_id[]" type="hidden" value="">\n' +
            '<input class="form-ghost" name="process_name[]" type="text"></div></td>\n' +
            '<td class="move"><a class="del-process pull-right"><i class="fa fa-trash"></i></a></td></tr>');
    });

    $("#formCustomerProcess").on('click','.del-process', function(event) {
        var target = event.currentTarget;
        var id = $(target).data('process_id');

        if (!id) {
            $(this).parent().parent().remove();
        } else {
            deleteRecord(id, actionDeleteProcess, communicate, '.tbody-process-customer > .row-', 'ten proces');
        }
    });

    $('.tbody-process-customer').sortable();


    /**
     * Bank process
     */

    $(".add-process").click(function() {
        $("#formBankProcess").append('<tr><td class="move"><i class="fa fa-arrows"></i></td>\n' +
            '<td><div class="form-flat"><input class="form-ghost" name="process[]" type="text" value=""></div></td>\n' +
            '<td class="move"><a class="del-process pull-right"><i class="fa fa-trash"></i></a></td></tr>');
    });

    $("#formBankProcess").on('click','.del-process', function() {
        $(this).parent().parent().remove();
    });

    $('.tbody-process').sortable();


    /**
     * DataTables
     */

    $('a[data-toggle="tab"]').on( 'shown.bs.tab', function (e) {
        $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
    });

    $('#data-tables').DataTable({
        columnDefs: [
            { type: 'natural', targets: 0 }
        ],
        "order": [],
        "bStateSave": true,
        "pageLength": 25,
        "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "Wszystkie"]],
        "language": {
            "sProcessing":   "Przetwarzanie...",
            "sLengthMenu":   "Pokaż _MENU_ pozycji",
            "sZeroRecords":  "Nie znaleziono pasujących pozycji",
            "sInfoThousands":  " ",
            "sInfo":         "Pozycje od _START_ do _END_ z _TOTAL_ łącznie",
            "sInfoEmpty":    "Pozycji 0 z 0 dostępnych",
            "sInfoFiltered": "(filtrowanie spośród _MAX_ dostępnych pozycji)",
            "sInfoPostFix":  "",
            "sSearch":       "Szukaj:",
            "sUrl":          "",
            "oPaginate": {
                "sFirst":    "|&lsaquo;",
                "sPrevious": "&laquo;",
                "sNext":     "&raquo;",
                "sLast":     "&rsaquo;|"
            },
            "sEmptyTable":     "Brak danych",
            "sLoadingRecords": "Wczytywanie...",
            "oAria": {
                "sSortAscending":  ": aktywuj, by posortować kolumnę rosnąco",
                "sSortDescending": ": aktywuj, by posortować kolumnę malejąco"
            }
        }
    });


    /**
     * Activate process
     */

    $('.check-active').click(function() {
        var self = $(this);
        var parent = $(self).parent();
        var id = $(parent).data('process');
        var cid = $(parent).data('cid');
        var bid = $(parent).data('bid');

        $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
        $.ajax({
            method: "POST",
            url: actionActivateProcess,
            data: { id : id, cid: cid, bid: bid },
            dataType: 'html',
            encode  : true,
            success: function (data) {

                $('.wheel-active-' + bid).children().each(function (index, value) {
                    $(value).removeClass('fa fa-check-square');
                    $(value).addClass('fa fa-square');
                });

                $(self).removeClass('fa fa-square');
                $(self).addClass('fa fa-check-square');

            },
            error: function(xhr, desc, err) {

                displayErrors(xhr, desc, err);
            }
        });
    });


    /**
     * Files
     */

    $('.add-file-process').on("change", function(e) {

        e.preventDefault();
        var self = $(this);

        var file = $(self).find('input[type="file"]').prop('files')[0];
        var cpid = $(self).data('cpid');
        var cid = $(self).data('cid');
        var bid = $(self).data('bid');

        var form_data = new FormData();
        form_data.append('file', file);
        form_data.append('customer_process_id', cpid);
        form_data.append('customer_id', cid);
        form_data.append('bank_id', bid);

        $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
        $.ajax({
            url: actionSaveFile, // point to server-side PHP script
            data: form_data,
            type: 'POST',
            contentType: false, // The content type used when sending data to the server.
            cache: false, // To unable request pages to be cached
            processData: false,
            success: function(data) {

                // console.log(data);

                if (communicate === true) {
                    $('.alert-process').html('<div class="alert alert-success" role="alert">' +
                        data.message + '</div>').fadeIn().fadeOut(3000);
                }

                $('.file-' + data.result.customer_process_id).append(
                    '<tr class="row-' + data.result.id + '"><td class="with-process">\n' +
                    '<a href="../../' + data.path + '" target="_blank">\n' + data.result.name + '</a></td><td>\n' +
                    '<button data-id="' + data.result.id + '" type="button" class="btn-flat-black pull-right file-delete">' +
                    '<i class="fa fa-trash" aria-hidden="true"></i></button></td></tr>'
                );
            }
        });
    });

    $("tbody#bodyFile").on('click','button.file-delete', function(event) {
        var target = event.currentTarget;
        var id = $(target).data('id');
        deleteRecord(id, actionDeleteFile, communicate, '#bodyFile > .row-', 'file');
    });


    /**
     * Note
     */

    $('#NoteModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var recipient = button.data('whatever'); // Extract info from data-* attributes
        var cpid = button.data('cpid');
        var id = button.data('id');
        var description = button.data('description');

        var modal = $(this);

        if (!id) {
            modal.find('.modal-body .note-id').val(0);
            modal.find('.modal-body .cpid').val(cpid);
            modal.find('.modal-title').text(recipient);
            modal.find('.modal-body .note-description').val('');
        } else {
            modal.find('.modal-body .note-id').val(id);
            modal.find('.modal-body .cpid').val(cpid);
            modal.find('.modal-title').text('Edycja notatki - ' + recipient);
            modal.find('.modal-body .note-description').val(description);
        }
    });

    $("tbody#bodyNote").on('click','button.note-delete', function(event) {
        var target = event.currentTarget;
        var id = $(target).data('id');
        deleteRecord(id, actionDeleteNote, communicate, '#bodyNote > .row-', 'note');
    });

    $('#saveNote').click(function () {
        saveNote(actionCreateNote, communicate);
    });
});


function saveColorProcess(id, color)
{
    $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
    $.ajax({
        type: "POST",
        url: actionSaveColorProcess,
        data: {id: id, color: color},
        success: function (data) {
            if (data === true) {
                $('#color-' + id).css('background-color', color);
            }
        },
        error: function (xhr, desc, err) {

            displayErrors(xhr, desc, err);
        }
    });
}

function saveNote(path, communicate)
{
    var id = $('.note-id').val();
    var cpid = $('.cpid').val();
    var active = $('.note-active').val();
    var description = $('.note-description').val();
    var sendData = '';

    if (!id) {
        sendData = {customer_process_id: cpid, description: description, active: active}
    } else {
        sendData = {id: id, customer_process_id: cpid, description: description, active: active}
    }

    if (description.length > 0) {
        $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
        $.ajax({
            type: "POST",
            url: path,
            data: sendData,
            success: function (data) {

                if (data.result.id) {
                    renderNote(data.result.id, data.result.customer_process_id, data.result.description);
                } else {
                    renderNote(id, cpid, description);
                }

                if (communicate === true) {
                    $('.alert-process').html('<div class="alert alert-success" role="alert">' +
                        data.message + '</div>').fadeIn().fadeOut(3000);
                }
            },
            error: function (xhr, desc, err) {

                displayErrors(xhr, desc, err);
            }
        });
    }
}

function renderNote(id, cpid, description)
{
    $('.note-id').val(id);
    var element = $('#bodyNote .row-' + id);

    if (element.length) {
        $(element).html(
            '<td class="with-process"><a class="btn-flat-black pull-left" data-toggle="modal" data-target="#NoteModal" data-id="' +
            id + '" data-cpid="' + cpid + '" data-description="' + description +
            '" title="' + description + '">' + description + '</a></td><td><button data-id="' +
            id + '" type="button" class="btn-flat-black pull-right note-delete">' +
            '<i class="fa fa-trash" aria-hidden="true"></i></button></td>'
        );
    } else {
        $('.note-' + cpid).append(
            '<tr class="row-' + id + '"><td class="with-process">' +
            '<a class="btn-flat-black pull-left" data-toggle="modal" data-target="#NoteModal" data-id="' +
            id + '" data-cpid="' + cpid + '" data-description="' +
            description + '" title="' + description + '">' + description +
            '</a></td><td><button data-id="' + id + '" type="button" class="btn-flat-black pull-right note-delete">' +
            '<i class="fa fa-trash" aria-hidden="true"></i></button></td></tr>'
        );
    }
}

function deleteRecord(id, path, communicate, element, label)
{
    if (confirmDelete(label)) {
        $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
        $.ajax({
            type: "POST",
            url: path + '/' + id,
            data: {id: id, _method: 'delete'},
            success: function (data) {

                if (communicate === true) {
                    $('.alert-process').html('<div class="alert alert-success" role="alert">' +
                        data.message + '</div>').fadeIn().fadeOut(3000);
                }

                $(element + id).fadeOut(300);
            },
            error: function (xhr, desc, err) {

                displayErrors(xhr, desc, err);
            }
        });
    }
}

function displayErrors(xhr, desc, err)
{
    var str = '';
    var errors = xhr.responseJSON;
    var text = xhr.responseText;

    if (errors && errors.errors) {

        $(errors.errors.name).each(function (i, v) {
            str += v + '\n';
        });

        $(errors.errors.email).each(function (i, v) {
            str += v + '\n';
        });

        alert(str);
    }

    if (text) {
        console.log(xhr.responseText);
    }

    if (desc && err) {
        console.log("Details : " + desc + "\nError : " + err);
    }
}

function confirmDelete(label)
{
    var str = 'ten element';

    if (label === 'file') {
        str = 'ten plik';
    } else
    if (label === 'note') {
        str = 'tą notatkę';
    } else
    if (label === 'bank') {
        str = 'ten bank';
    } else {
        str = label;
    }

    var answer = confirm("Czy na pewno chcesz usunąć " + str + " ?");

    if (answer) {
        return true;
    } else {
        return false;
    }
}
