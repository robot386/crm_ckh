@extends('layouts.credit')

@section('content')

    <div class="container pager">

        <div class="row">
            <div class="col-md-12">

                @include('partials.status')

                {!! Form::model($browser, ['method' => 'PATCH', 'action' => ['BrowsersController@update', $browser->id], 'files' => true]) !!}

                <h5 class="pull-left">Edycja przeglądarki: #{{ $browser->id }} - {{ $browser->name }}</h5>

                <div class="form-group text-right">
                    {!! Form::submit('Zapisz', ['class'=>'btn btn-outline-primary']) !!}
                    {!! link_to(URL::previous(), 'Powrót', ['class' => 'btn btn-outline-primary']) !!}
                </div>

                <div class="row">
                    <div class="col-md">

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            {!! Form::label('name', "Nazwa:") !!}
                            {!! Form::text('name', null,
                                ['class' => 'form-control', 'placeholder' => 'Nazwa']) !!}

                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('name') }}</strong>
                                </span>
                            @endif

                        </div>

                    </div>
                    <div class="col-md">

                        <div class="form-group{{ $errors->has('os') ? ' has-error' : '' }}">
                            {!! Form::label('os', "System operacyjny:") !!}
                            {!! Form::select('os',
                                [
                                    '-- Wybierz --',
                                    'Windows' => 'Windows',
                                    'OS X' => 'Mac OS X',
                                    'Linux' => 'Linux',
                                ],
                                null, ['class' => 'form-control']) !!}

                            @if ($errors->has('os'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('os') }}</strong>
                                </span>
                            @endif

                        </div>

                    </div>
                </div>

                <div class="form-group{{ $errors->has('path') ? ' has-error' : '' }}">
                    {!! Form::label('path', "Ścieżka do programu:") !!}
                    {!! Form::text('path', null,
                        ['class' => 'form-control', 'placeholder' => 'C:\Program Files\Internet Explorer\iexplore.exe']) !!}

                    @if ($errors->has('path'))
                        <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('path') }}</strong>
                        </span>
                    @endif

                </div>

                <div class="form-group text-right">
                    {!! Form::submit('Zapisz', ['class'=>'btn btn-outline-primary']) !!}
                    {!! link_to(URL::previous(), 'Powrót', ['class' => 'btn btn-outline-primary']) !!}
                </div>

                {!! Form::close() !!}

            </div>
        </div>

    </div>

@endsection
