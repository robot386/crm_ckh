@extends('layouts.credit')

@section('content')

    <div class="container pager">

        <div class="row">
            <div class="col-md-12">

                @include('partials.status')
                @include('partials.errors')
                @include('partials.config_menu')

                <div class="pull-right my-2">
                    <a class="btn btn-success btn-sm" href="{{ route('valuation.create') }}">Dodaj nową wycenę</a>
                </div>

                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-sm" cellspacing="0" width="100%">
                        <thead class="thead-dark">
                            <tr>
                                <th colspan="2" scope="col">Nazwa</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($valuations as $item)

                                <tr>
                                    <td>
                                        <a href="{{ url('configuration/valuation/edit') . '/' . $item->id }}">
                                            {{ $item->name }}
                                        </a>
                                    </td>
                                    <td style="width: 1rem;">

                                        {!! Form::open(['url' => 'configuration/valuation/delete/' . $item->id,
                                            'onsubmit' => 'return confirmDelete("tą wycenę");']) !!}

                                        {!! Form::hidden('_method', 'DELETE') !!}
                                        {!! Form::button('<i class="fa fa-trash" aria-hidden="true"></i>',
                                            ['type' => 'submit', 'class' => 'btn btn-link px-2 py-0']) !!}

                                        {!! Form::close() !!}

                                    </td>
                                </tr>

                            @endforeach

                        </tbody>
                    </table>
                </div>

            </div>
        </div>

    </div>

@endsection
