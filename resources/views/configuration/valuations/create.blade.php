@extends('layouts.credit')

@section('content')

    <div class="container pager">

        <div class="row">
            <div class="col-md-12">

                @include('partials.status')

                {!! Form::open(['route' => 'valuation.store']) !!}

                <h5 class="pull-left">Nowa wycena</h5>

                <div class="form-group text-right">
                    {!! Form::submit('Zapisz', ['class'=>'btn btn-outline-primary']) !!}
                    {!! link_to(URL::previous(), 'Powrót', ['class' => 'btn btn-outline-primary']) !!}
                </div>

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    {!! Form::label('name', "Nazwa:") !!}
                    {!! Form::text('name', null,
                        ['class' => 'form-control', 'placeholder' => 'Nazwa']) !!}

                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('name') }}</strong>
                        </span>
                    @endif

                </div>

                <div class="form-group text-right">
                    {!! Form::submit('Zapisz', ['class'=>'btn btn-outline-primary']) !!}
                    {!! link_to(URL::previous(), 'Powrót', ['class' => 'btn btn-outline-primary']) !!}
                </div>

                {!! Form::close() !!}

            </div>
        </div>

    </div>

@endsection
