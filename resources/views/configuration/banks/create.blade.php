@extends('layouts.credit')

@section('content')

    <div class="container pager">

        <div class="row">
            <div class="col-md-12">

                @include('partials.status')

                {!! Form::open(['route' => 'bank.store', 'files' => true]) !!}

                <h5 class="pull-left">Nowy bank</h5>

                <div class="form-group text-right">
                    {!! Form::submit('Zapisz', ['class'=>'btn btn-outline-primary']) !!}
                    {!! link_to(URL::previous(), 'Powrót', ['class' => 'btn btn-outline-primary']) !!}
                </div>

                <div class="row">
                    <div class="col-md">

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            {!! Form::label('name', "Nazwa:") !!}
                            {!! Form::text('name', null,
                                ['class' => 'form-control', 'placeholder' => 'Nazwa']) !!}

                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('name') }}</strong>
                                </span>
                            @endif

                        </div>

                    </div>
                    <div class="col-md">

                        <div class="form-group{{ $errors->has('browser_id') ? ' has-error' : '' }}">
                            {!! Form::label('browser_id', "Przeglądarka:") !!}
                            {!! Form::select('browser_id', $browsers, null, ['class' => 'form-control']) !!}

                            @if ($errors->has('browser_id'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('browser_id') }}</strong>
                                </span>
                            @endif

                        </div>

                    </div>
                </div>

                <div class="form-group{{ $errors->has('url') ? ' has-error' : '' }}">
                    {!! Form::label('url', "Adres internetowy:") !!}
                    {!! Form::url('url', null,
                        ['class' => 'form-control', 'placeholder' => 'https://nowybank.com']) !!}

                    @if ($errors->has('url'))
                        <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('url') }}</strong>
                        </span>
                    @endif

                </div>

                <div class="row">
                    <div class="col-md">

                        <div class="form-group{{ $errors->has('logo') ? ' has-error' : '' }}">
                            {!! Form::label('logo', "Logo:") !!}
                            {!! Form::file('logo', ['class' => 'form-control-file']) !!}

                            @if ($errors->has('logo'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('logo') }}</strong>
                                </span>
                            @endif

                        </div>

                    </div>
                    <div class="col-md">

                        <div class="form-group{{ $errors->has('contact_file') ? ' has-error' : '' }}">
                            {!! Form::label('contact_file', "Plik z kontaktami:") !!}
                            {!! Form::file('contact_file', ['class' => 'form-control-file']) !!}

                            @if ($errors->has('contact_file'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('contact_file') }}</strong>
                                </span>
                            @endif

                        </div>

                    </div>
                </div>

                <h5>Procesy</h5>

                <table class="table table-bordered table-hover table-flat table-striped" cellspacing="0" width="100%" id="formBankProcess">
                    <thead>
                        <tr class="thead-dark">
                            <th><i class="fa fa-arrows"></i></th>
                            <th>Nazwa</th>
                            <th>
                                <a class="add-process pull-right"><i class="fa fa-plus-circle"></i></a>
                            </th>
                        </tr>
                    </thead>
                    <tbody class="tbody-process">
                        <tr>
                            <td class="move"><i class="fa fa-arrows"></i></td>
                            <td>
                                <div class="form-flat{{ $errors->has('process') ? ' has-error' : '' }}">
                                    {!! Form::text('process[]', null, ['class' => 'form-ghost']) !!}

                                    @if ($errors->has('process'))
                                        <span class="help-block">
                                            <strong class="text-danger">{{ $errors->first('process') }}</strong>
                                        </span>
                                    @endif

                                </div>
                            </td>
                            <td class="move">
                                <a class="del-process pull-right"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <div class="clearfix"></div>

                <div class="form-group text-right">
                    {!! Form::submit('Zapisz', ['class'=>'btn btn-outline-primary']) !!}
                    {!! link_to(URL::previous(), 'Powrót', ['class' => 'btn btn-outline-primary']) !!}
                </div>

                {!! Form::close() !!}

            </div>
        </div>

    </div>

@endsection
