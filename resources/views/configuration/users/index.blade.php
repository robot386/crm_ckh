@extends('layouts.credit')

@section('content')

    <div class="container pager">

        <div class="row">
            <div class="col-md-12">

                @include('partials.status')
                @include('partials.errors')
                @include('partials.config_menu')

                <div class="pull-right my-2">
                    <a class="btn btn-success btn-sm" href="{{ route('user.create') }}">Dodaj nowego użytkownika</a>
                </div>

                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-sm" cellspacing="0" width="100%">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">Nazwa</th>
                                <th scope="col">E-mail</th>
                                <th colspan="2" scope="col">Role</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($users as $item)

                                <tr>
                                    <td>
                                        <a href="{{ url('configuration/user/edit') . '/' . $item->id }}">
                                            {{ $item->name }}
                                        </a>
                                    </td>
                                    <td>{{ $item->email }}</td>
                                    <td>
                                        @foreach ($item->roles as $role)

                                            <span class="badge badge-pill badge-{{
                                                $role->name == 'Admin' ? 'danger'
                                                    : ($role->name == 'Moderator' ? 'primary'
                                                        : ($role->name == 'User' ? 'success' : '')
                                                    )
                                                }}">
                                                {{ $role->name }}
                                            </span>

                                        @endforeach
                                    </td>
                                    <td style="width: 1rem;">

                                        @if (Auth::user()->id != $item->id)

                                            {!! Form::open(['url' => 'configuration/user/delete/' . $item->id,
                                                'onsubmit' => 'return confirmDelete("tego użytkownika");']) !!}

                                            {!! Form::hidden('_method', 'DELETE') !!}
                                            {!! Form::button('<i class="fa fa-trash" aria-hidden="true"></i>',
                                                ['type' => 'submit', 'class' => 'btn btn-link px-2 py-0']) !!}

                                            {!! Form::close() !!}
                                            
                                        @endif

                                    </td>
                                </tr>

                            @endforeach

                        </tbody>
                    </table>
                </div>

            </div>
        </div>

    </div>

@endsection
