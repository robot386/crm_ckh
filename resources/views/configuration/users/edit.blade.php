@extends('layouts.credit')

@section('content')

    <div class="container pager">

        <div class="row">
            <div class="col-md-12">

                @include('partials.status')

                {!! Form::model($user, ['method' => 'PATCH', 'action' => ['UsersController@update', $user->id], 'files' => true]) !!}

                <h5 class="pull-left">Edycja celu kredytu: #{{ $user->id }} - {{ $user->name }}</h5>

                <div class="form-group text-right">
                    {!! Form::submit('Zapisz', ['class'=>'btn btn-outline-primary']) !!}
                    {!! link_to(URL::previous(), 'Powrót', ['class' => 'btn btn-outline-primary']) !!}
                </div>

                <div class="row">
                    <div class="col-md">

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            {!! Form::label('name', "Nazwa:") !!}
                            {!! Form::text('name', null,
                                ['class' => 'form-control', 'placeholder' => 'Nazwa']) !!}

                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('name') }}</strong>
                                </span>
                            @endif

                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            {!! Form::label('email', "E-mail:") !!}
                            {!! Form::text('email', null,
                                ['class' => 'form-control', 'placeholder' => 'E-mail']) !!}

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('email') }}</strong>
                                </span>
                            @endif

                        </div>

                    </div>
                    <div class="col-md">

                        <div class="form-group{{ $errors->has('roles') ? ' has-error' : '' }}">
                            {!! Form::label('roles', "Rola:") !!}
                            {!! Form::select(
                                'roles',
                                $roles,
                                null,
                                [
                                    'multiple'=>'multiple',
                                    'class' => 'form-control',
                                    'name' => 'roles[]'
                                ])
                            !!}

                            @if ($errors->has('roles'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('roles') }}</strong>
                                </span>
                            @endif

                        </div>

                    </div>
                </div>

                <div class="row">
                    <div class="col-md">

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            {!! Form::label('password', "Hasło:") !!}
                            {!! Form::password('password',
                                ['class' => 'form-control', 'placeholder' => 'Hasło']) !!}

                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('password') }}</strong>
                                </span>
                            @endif

                        </div>

                    </div>
                    <div class="col-md">

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            {!! Form::label('password_confirmation', "Powtórz hasło:") !!}
                            {!! Form::password('password_confirmation',
                                ['class' => 'form-control', 'placeholder' => 'Powtórz hasło']) !!}

                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            @endif

                        </div>

                    </div>
                </div>

                <div class="form-group text-right">
                    {!! Form::submit('Zapisz', ['class'=>'btn btn-outline-primary']) !!}
                    {!! link_to(URL::previous(), 'Powrót', ['class' => 'btn btn-outline-primary']) !!}
                </div>

                {!! Form::close() !!}

            </div>
        </div>

    </div>

@endsection
