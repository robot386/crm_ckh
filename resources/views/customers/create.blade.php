@extends('layouts.credit')

@section('content')

    <div class="container pager" id="newCustomerForm">

        <div class="row">
            <div class="col-md-12">

                <div class="alert-process"></div>

                @include('partials.customers.create')

            </div>
        </div>

    </div>

@endsection
