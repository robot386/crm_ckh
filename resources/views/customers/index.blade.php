@extends('layouts.credit')

@section('content')

    <div class="container pager">

        <div class="row">
            <div class="col-md-12">

                @include('partials.status')
                @include('partials.errors')

                <div class="table-responsive">
                    <table id="data-tables" class="table table-bordered table-hover table-sm" cellspacing="0" width="100%">

                        <thead class="{{ Route::is('customers.archive') ? 'bg-secondary text-white' : 'thead-dark' }}">

                            <tr>
                                <th scope="col">NAZWISKO I IMIĘ</th>
                                <th scope="col">NASTĘPNA UMOWA</th>
                                <th scope="col">NASTĘPNA WYPŁATA</th>
                                <th scope="col">WNIOSKI KREDYTOWE</th>
                            </tr>

                        </thead>
                        <tbody>

                            @foreach ($customers as $item)

                                <tr>
                                    <th>
                                        <a href="{{ url('customer/process') . '/' . $item->id }}#content4ProcessBank">{{ $item->name }}</a>
                                    </th>
                                    <td>{{ date('d-m-Y', strtotime($item->next_contract_date)) }}</td>
                                    <td>{{ $item->credit ? date('d-m-Y', strtotime($item->credit->payment_date)) : 'Brak' }}</td>
                                    <td>

                                        <table class="table-small">

                                            @if (count($item->process))

                                                @foreach ($item->getDistinctProcesses($item->process, 1) as $process)

                                                    <tr class="{{ !$process->status ? 'color-archive' : '' }}">
                                                        <td>
                                                            ({{ $process->bank_prefer ?: 0 }})
                                                        </td>
                                                        <td>
                                                            <div class="wheel" style="background-color: {{ $process->active && $process->color ? $process->color . ';' : '#ccc;' }}"></div>
                                                        </td>
                                                        <td>

                                                            @if ($process->bank_id)

                                                                <a href="{{ url('customer/process') . '/' .  $item->id . '#content4ProcessBank' }}">
                                                                    {{ $process->bank->name }}
                                                                </a>

                                                            @else

                                                                <p class="text-info mb-0">Brak banku</p>

                                                            @endif

                                                        </td>
                                                        <td>
                                                            @if ($process->active)

                                                                {{ $process->name }}

                                                            @else

                                                                <p class="text-info mb-0">Brak aktywnego procesu</p>

                                                            @endif
                                                        </td>
                                                    </tr>

                                                @endforeach

                                            @else

                                                <tr>
                                                    <td>
                                                        <a href="{{ url('customer/process') . '/' .  $item->id . '#content4ProcessBank'}}">
                                                            <p class="text-info mb-0">Brak preferowanych banków</p>
                                                        </a>
                                                    </td>
                                                </tr>

                                            @endif

                                        </table>

                                    </td>
                                </tr>

                            @endforeach

                        </tbody>
                    </table>
                </div>

            </div>
        </div>

    </div>

@endsection
