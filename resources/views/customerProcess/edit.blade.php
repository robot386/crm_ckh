@extends('layouts.credit')

@section('content')

    <div class="container pager" id="content4ProcessBank">

        <div class="row">
            <div class="col-md-12">

                @include('partials.status')
                @include('partials.errors')

                {!! Form::open(['url' => 'customer/process/update']) !!}
                {!! Form::hidden('cid', $process[0]->customer_id) !!}
                {!! Form::hidden('bid', $process[0]->bank_id) !!}

                <h5 class="pull-left">Procesy kredytobiorcy: #{{ $process[0]->customer_id }} - {{ $process[0]->customer->name }}
                    dla banku {{ $process[0]->bank->name }}</h5>

                <div class="form-group text-right">
                    {!! Form::submit('Zapisz', ['class'=>'btn btn-outline-primary']) !!}
                    {!! link_to(URL::previous(), 'Powrót', ['class' => 'btn btn-outline-primary']) !!}
                </div>

                <table class="table table-bordered table-hover table-flat table-striped" cellspacing="0" width="100%" id="formCustomerProcess">
                    <thead>
                        <tr class="thead-dark">
                            <th><i class="fa fa-arrows"></i></th>
                            <th>Nazwa</th>
                            <th>
                                <a class="add-process-customer pull-right"><i class="fa fa-plus-circle"></i></a>
                            </th>
                        </tr>
                    </thead>
                    <tbody class="tbody-process-customer">

                    @foreach ($process as $i => $item)

                        <tr class="row-{{ $item->id }}">
                            <td class="move"><i class="fa fa-arrows"></i></td>
                            <td>
                                <div class="form-flat{{ $errors->has('process_name') ? ' has-error' : '' }}">
                                    {!! Form::hidden('process_id[]', $item->id) !!}
                                    {!! Form::text('process_name[]', $item->name, ['class' => 'form-ghost']) !!}

                                    @if ($errors->has('process_name'))
                                        <span class="help-block">
                                            <strong class="text-danger">{{ $errors->first('process_name') }}</strong>
                                        </span>
                                    @endif

                                </div>
                            </td>
                            <td class="move">
                                <a class="del-process pull-right" data-process_id="{{ $item->id }}"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>

                    @endforeach

                    </tbody>
                </table>

                <div class="form-group text-right">
                    {!! Form::submit('Zapisz', ['class'=>'btn btn-outline-primary']) !!}
                    {!! link_to(URL::previous(), 'Powrót', ['class' => 'btn btn-outline-primary']) !!}
                </div>

                {!! Form::close() !!}

            </div>
        </div>

    </div>

@endsection

