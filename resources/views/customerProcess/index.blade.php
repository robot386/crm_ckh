@extends('layouts.credit')

@section('content')

    <div class="container pager" id="content4ProcessBank">

        <div class="row">
            <div class="col-md-12">

                @include('partials.status')
                @include('partials.errors')

                <div class="alert-process">
                    @include('partials.exist')
                </div>

                @include('partials.customers.carousel')

                @include('partials.customers.tranche')

                <hr>

                @include('partials.customers.editProcess')

                <hr>

                @include('partials.customers.partner')

                <hr>

                @include('partials.customers.bank')

            </div>
        </div>

    </div>

    @include('partials.customers.noteModal')

    @include('partials.customers.bankModal')

@endsection

