@extends('layouts.credit')

@section('content')

    <div class="container pager" id="LogActivity">

        <div class="row">
            <div class="col-md-12">

                @include('partials.status')
                @include('partials.errors')

                <h5>Lista aktywnych sesji</h5>

                <table class="table table-bordered" style="font-size: .8rem;">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Użytkownik</th>
                        <th>System operacyjny</th>
                        <th>Przeglądarka</th>
                        <th>Ostatnia aktywność</th>
                    </tr>
                    </thead>
                    <tbody>

                    @if (count($ses))

                        @foreach($ses as $key => $session)

                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>{{ $session['name'] }}</td>
                                <td>{{ $session['platform'] }}</td>
                                <td>{{ $session['browser'] }}</td>
                                <td>{{ $session['time'] }}</td>
                            </tr>

                        @endforeach

                    @endif

                    </tbody>
                </table>

                <h5>Lista aktywności</h5>

                <table class="table table-bordered" style="font-size: .8rem;">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Tytuł</th>
                            <th>URL</th>
                            <th>Metoda</th>
                            <th>IP</th>
                            <th>System operacyjny</th>
                            <th>Przeglądarka</th>
                            <th>Użytkownik</th>
                        </tr>
                    </thead>
                    <tbody>

                        @if (count($activity))

                            @foreach($activity as $key => $log)

                                <tr>
                                    <td>{{ ++$key }}</td>
                                    <td>{{ $log['title'] }}</td>
                                    <td class="text-success">{{ $log['url'] }}</td>
                                    <td><label class="badge badge-info">{{ $log['method'] }}</label></td>
                                    <td class="text-primary">{{ $log['ip'] }}</td>
                                    <td class="text-secondary">{{ $log['platform'] }}</td>
                                    <td class="text-danger">{{ $log['browser'] }}</td>
                                    <td>{{ $log['name'] }}</td>
                                </tr>

                            @endforeach

                        @endif

                    </tbody>
                </table>

            </div>
        </div>

    </div>

@endsection

