<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="robots" content="noindex, nofollow" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800&subset=latin,latin-ext' rel='stylesheet' type='text/css' />

    <title>{{ config('app.name', 'Kredyty') }}</title>

    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.formatCurrency-1.4.0.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.formatCurrency.all.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function()
        {
            /**
             * Price format
             */

            $('.number').blur(function()
            {
                $('.number').formatCurrency({ colorize:true, region: 'pl-PL' });
            });
        });
    </script>

    <!-- Styles -->
    {{--<link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.16/datatables.min.css"/>

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jexcel/1.3.4/css/jquery.jexcel.min.css" />

    <link rel="stylesheet" type="text/css" href="http://bgrins.github.io/spectrum/spectrum.css"/>

    <link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">

    <link href="{{ asset('css/datepicker.css') }}" rel="stylesheet">

    <link href="{{ asset('css/main.css') }}" rel="stylesheet">

</head>
<body>

<div class="container">
    <header class="row header-wraper">

        <div class="col-md-4">

            @auth

                @include('partials.menu')

            @endauth

        </div>

        <div class="col-md-4">
            <div class="logo-wrapper loaded">
                <a href="{{ url('/') }}" title="Jacek Kur" class="logo">
                    <img class="img-fluid" src="{{ asset('images/main_logo.png') }}" alt="Jacek Kur" />
                </a>
            </div>
        </div>

        <div class="col-md-4">

            @auth

                <ul class="nav-next-bootom nav justify-content-end">
                    <li class="nav-item" style="padding: .5rem;">
                        <span>{{ Auth::user()->name }}</span>
                        <span class="badge badge-pill badge-{{ \App\Session::switchStatus()['users']['color'] }}">
                            <a class="text-white" href="{{ url('log/activity') }}">
                                {{ \App\Session::switchStatus()['users']['count'] }}
                            </a>
                        </span>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link btn btn-outline-primary" href="{{ route('logout') }}"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            WYLOGUJ
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>

            @endauth

        </div>

    </header>
</div>

@yield('content')

<div class="container">
    <footer class="footer text-center">
        &copy; {{ date("Y") }} {{ config('app.name', 'Kredyty') }}
    </footer>
</div>

<!-- Scripts -->
{{--<script src="{{ asset('js/app.js') }}"></script>--}}

<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.16/datatables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.16/sorting/natural.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/ui/1.11.1/jquery-ui.js"></script>

<script src="{{ asset('js/owl.carousel.min.js') }}"></script>
<script src="http://bgrins.github.io/spectrum/spectrum.js"></script>

<script src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('js/bootstrap-datepicker-pl.js') }}" charset="UTF-8"></script>
<script src="{{ asset('js/jquery.jexcel.js') }}"></script>

<script>
    var communicate = false;
    var url = document.location.href;
    var actionDeleteFile4Customer = "{{ url('customer/file/delete') }}";
    var action4Partner = "{{ route('customer.partner.create') }}";
    var delete4Partner = "{{ url('customer/partner/delete') }}";
    var actionSaveDate = "{{ url('customer/process/date') }}";
    var actionSaveColorProcess = "{{ url('customer/process/color') }}";
    var actionGetDataSpreadsheet = "{{ url('customer/tranche') }}";
    var actionSaveSpreadsheet = "{{ url('customer/tranches/store') }}";
    var actionActivateProcess = "{{ route('customer.process.activate') }}";
    var actionSaveFile = "{{ url('customer/process/file/store') }}";
    var actionDeleteFile = "{{ url('customer/process/file/delete') }}";
    var actionDeleteNote = "{{ url('customer/process/note/delete') }}";
    var actionCreateNote = "{{ url('customer/process/note/store') }}";
    var actionSetBankPrefer = "{{ url('customer/process/bank/prefer') }}";
    var actionChangePaymentDate = "{{ url('customer/change/payment') }}";
    var actionChangeAgreementDate = "{{ url('customer/change/agreement') }}";
    var actionDeleteBank = "{{ url('customer/process/bank/delete') }}";
    var actionDeleteProcess = "{{ url('customer/process/delete') }}";
    var customerId = "{{ $customer->id ?? 0 }}";
    var partnerId = "{{ $partner->id ?? 0 }}";
    var routeProcess = "{{ Route::is('customer.process') }}";
</script>

<script src="{{ asset('js/main.js') }}"></script>

@if (Route::is('customer.process'))

<script>
    $(document).ready(function () {

        /**
         * Excel
         */

        var dataExcel = $.fn.jexcel('helper', { action:'createEmptyData', cols:6, rows:6 });
        var elementExcel = $('#transze');
        var idExcel = $(elementExcel).data('id');
        var bankExcel = $(elementExcel).data('bank');
        var customerExcel = $(elementExcel).data('customer');

        var newDataExcel = idExcel ? getDataSpreadsheet(idExcel, elementExcel) : dataExcel;

        $(elementExcel).jexcel({
            data: newDataExcel
        });

        $(elementExcel).on('change', function () {
            var data = $(elementExcel).jexcel('getData');
            var newDate = JSON.stringify(data);

            saveSpreadsheet(elementExcel, newDate, bankExcel, customerExcel);
        });
    });

    function getDataSpreadsheet(id, element)
    {
        $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
        $.ajax({
            type: "GET",
            url: actionGetDataSpreadsheet + '/' + id,
            success: function (data) {

                $(element).attr('data-id', id);

                var str = JSON.parse(data);

                $(element).jexcel({
                    data: str
                });
            },
            error: function (xhr, desc, err) {

                displayErrors(xhr, desc, err);
            }
        });
    }

    function saveSpreadsheet(element, data, bank, customer)
    {
        var newData = '';
        var id = $(element).attr('data-id');

        if (!id) {
            newData = {customer_id: customer, bank_id: bank, description: data};
        } else {
            newData = {customer_id: customer, id: id, bank_id: bank, description: data};
        }

        $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
        $.ajax({
            type: "POST",
            url: actionSaveSpreadsheet,
            data: newData,
            success: function (data) {

                $(element).attr('data-id', data.result.id);
            },
            error: function (xhr, desc, err) {

                displayErrors(xhr, desc, err);
            }
        });
    }
</script>

@endif

</body>
</html>