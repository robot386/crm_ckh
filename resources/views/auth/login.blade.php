@extends('layouts.credit')

@section('content')

    <div class="container pager">

        <form class="form-horizontal" method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}

            <div class="row">
                <div class="col-md-6 offset-md-3">

                    <h4>Logowanie</h4>
                    <hr>

                </div>
            </div>

            <div class="row">
                <div class="col-md-6 offset-md-3">

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="control-label" style="display: none;">E-Mail</label>

                        <div class="input-group">

                            <div class="input-group-prepend">
                                <span class="input-group-text" id="inputGroupPrepend2"><i class="fa fa-at"></i></span>
                            </div>

                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}"
                                   required autofocus placeholder="E-mail">

                        </div>
                    </div>

                </div>
                <div class="col-md-3">

                    @if ($errors->has('email'))

                        <div class="form-control-feedback">
                            <span class="text-danger align-middle">
                                <i class="fa fa-close"></i> {{ $errors->first('email') }}
                            </span>
                        </div>

                    @endif

                </div>
            </div>

            <div class="row">
                <div class="col-md-6 offset-md-3">

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="control-label" style="display: none;">Hasło</label>

                        <div class="input-group">

                            <div class="input-group-prepend">
                                <span class="input-group-text" id="inputGroupPrepend2"><i class="fa fa-key"></i></span>
                            </div>

                            <input id="password" type="password" class="form-control" name="password"
                                   required placeholder="Hasło">

                        </div>
                    </div>

                </div>
                <div class="col-md-3">

                    @if ($errors->has('password'))

                        <div class="form-control-feedback">
                            <span class="text-danger align-middle">
                                <i class="fa fa-close"></i> {{ $errors->first('password') }}
                            </span>
                        </div>

                    @endif

                </div>
            </div>

            <div class="row">
                <div class="col-md-6 offset-md-3">

                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Zapamiętaj mnie
                            </label>
                        </div>
                    </div>

                </div>
            </div>

            <div class="row">
                <div class="col-md-6 offset-md-3">

                    <div class="form-group">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-sign-in"></i> Zaloguj
                        </button>

                        <a class="btn btn-link" href="{{ route('password.request') }}">
                            Przypomnij hasło
                        </a>
                    </div>

                </div>
            </div>

        </form>

    </div>

@endsection