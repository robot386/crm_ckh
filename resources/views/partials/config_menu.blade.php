<ul class="nav nav-pills nav-fill">

    <li class="nav-item">
        <a class="nav-link btn btn-outline-primary {{ Route::is('banks') ? 'active' : null }}" href="{{ route('banks') }}">
            BANKI
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link btn btn-outline-primary {{ Route::is('browsers') ? 'active' : null }}" href="{{ route('browsers') }}">
            PRZEGLĄDARKI
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link btn btn-outline-primary {{ Route::is('purposes') ? 'active' : null }}" href="{{ route('purposes') }}">
            CEL KREDYTU
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link btn btn-outline-primary {{ Route::is('purchases') ? 'active' : null }}" href="{{ route('purchases') }}">
            TRYB ZAKUPU
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link btn btn-outline-primary {{ Route::is('garages') ? 'active' : null }}" href="{{ route('garages') }}">
            GARAŻ
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link btn btn-outline-primary {{ Route::is('valuations') ? 'active' : null }}" href="{{ route('valuations') }}">
            WYCENA NIERUCHOMOŚCI
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link btn btn-outline-primary {{ Route::is('users') ? 'active' : null }}" href="{{ route('users') }}">
            UŻYTKOWNICY
        </a>
    </li>

</ul>