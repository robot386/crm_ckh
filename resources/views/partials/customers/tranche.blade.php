<h5 class="head-toggle-tranche mt-3">Transze kredytu</h5>

<div id="transze"
     class="element-toggle-tranche mb-3"
     data-id="{{ $tranche->id ?? 0 }}"
     data-bank="{{ $processes[0]->bank_id ?? 0 }}"
     data-customer="{{ $processes[0]->customer_id ?? 0 }}">
</div>