<div class="modal fade" id="NoteModal" tabindex="-1" role="dialog" aria-labelledby="NoteModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="NoteModalLabel">Tytuł</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="alert-note"></div>

            {!! Form::open() !!}

                <div class="modal-body">

                    {!! Form::hidden('id', null, ['class' => 'note-id']) !!}
                    {!! Form::hidden('customer_process_id', null, ['class' => 'cpid']) !!}
                    {!! Form::hidden('active', 1, ['class' => 'note-active']) !!}

                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                        {!! Form::label('description', "Treść:") !!}
                        {!! Form::textarea('description', null, ['class' => 'form-control note-description']) !!}

                        @if ($errors->has('description'))
                            <span class="help-block">
                                <strong class="text-danger">{{ $errors->first('description') }}</strong>
                            </span>
                        @endif

                    </div>

                </div>
                <div class="modal-footer">
                    {!! Form::button('Zamknij', ['class'=>'btn btn-outline-primary', 'data-dismiss' => 'modal']) !!}
                    {!! Form::button('Zapisz', ['id' => 'saveNote', 'class'=>'btn btn-outline-primary', 'data-dismiss' => 'modal']) !!}
                </div>

            {!! Form::close() !!}

        </div>
    </div>
</div>