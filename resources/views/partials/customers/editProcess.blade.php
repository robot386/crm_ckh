
    <h5 class="pull-left head-toggle-customer">Kredytobiorca</h5>

    <div class="form-group text-right">

        @if ($customer->active)
            {!! link_to(url('customer/deactivate') . '/' . $customer->id, 'Archiwizuj', ['class' => 'btn btn-outline-primary']) !!}
        @else

            {!! link_to(url('customer/activate') . '/' . $customer->id, 'Przywróć', ['class' => 'btn btn-outline-primary pull-right ml-2']) !!}

            {!! Form::open(['url' => 'customer/delete/' . $customer->id,
                                     'onsubmit' => 'return confirmDelete("tego kredytobiorcę");']) !!}

            {!! Form::hidden('_method', 'DELETE') !!}
            {!! Form::button('Usuń', ['type' => 'submit', 'class' => 'btn btn-outline-primary']) !!}

            {!! Form::close() !!}

        @endif

    </div>

    {!! Form::model($customer, ['id' => 'updateCustomerForm', 'method' => 'PATCH', 'action' => ['CustomersController@updateOnBank', $customer->id]]) !!}

    <ul id="table-sortable" class="element-toggle-customer">
        <li>
            <table class="table table-bordered table-hover table-flat table-striped table-toggle open" cellspacing="0" width="100%">
                <thead>
                    <tr class="thead-dark">
                        <th colspan="2" class="current-name">
                            {{ $customer->name }}
                        </th>
                    </tr>
                </thead>
                <tbody>
                <tr>
                    <th class="table-secondary" scope="row">{!! Form::label('name', "Nazwisko i imię:") !!}
                        <span class="text-danger pull-right">*</span>
                    </th>
                    <td>
                        <div class="form-flat{{ $errors->has('name') ? ' has-error' : '' }}">
                            {!! Form::text('name', null, ['class' => 'form-ghost name-customer']) !!}

                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('name') }}</strong>
                                </span>
                            @endif

                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="table-secondary" scope="row">{!! Form::label('phone', "Telefon:") !!}</th>
                    <td>
                        <div class="form-flat{{ $errors->has('phone') ? ' has-error' : '' }}">
                            {!! Form::text('phone', null, ['class' => 'form-ghost']) !!}

                            @if ($errors->has('phone'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('phone') }}</strong>
                                </span>
                            @endif

                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="table-secondary" scope="row">{!! Form::label('email', "E-mail:") !!}
                        <span class="text-danger pull-right">*</span>
                    </th>
                    <td>
                        <div class="form-flat{{ $errors->has('email') ? ' has-error' : '' }}">
                            {!! Form::text('email', null, ['class' => 'form-ghost']) !!}

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('email') }}</strong>
                                </span>
                            @endif

                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="table-secondary" scope="row">{!! Form::label('address', "Adres:") !!}</th>
                    <td>
                        <div class="form-flat{{ $errors->has('address') ? ' has-error' : '' }}">
                            {!! Form::text('address', null, ['class' => 'form-ghost']) !!}

                            @if ($errors->has('address'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('address') }}</strong>
                                </span>
                            @endif

                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="table-secondary" scope="row">
                        {!! Form::label('next_contract_date', "Data następnej umowy:") !!}
                    </th>
                    <td>
                        <div class="form-flat{{ $errors->has('next_contract_date') ? ' has-error' : '' }}">
                            {!! Form::text(
                                'next_contract_date',
                                $customer->next_contract_date
                                ? date('d-m-Y', strtotime($customer->next_contract_date))
                                : null,
                                ['class' => 'form-ghost datepicker']
                            ) !!}

                            @if ($errors->has('next_contract_date'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('next_contract_date') }}</strong>
                                </span>
                            @endif

                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="table-secondary" scope="row">
                        {!! Form::label('source_customer_description', "Źródło pozyskania klienta:") !!}
                    </th>
                    <td>
                        <div class="form-flat{{ $errors->has('source_customer_description') ? ' has-error' : '' }}">
                            {!! Form::textarea('source_customer_description', null, ['class' => 'form-ghost']) !!}

                            @if ($errors->has('source_customer_description'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('source_customer_description') }}</strong>
                                </span>
                            @endif

                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="table-secondary" scope="row">Data dodania klienta:</th>
                    <td>{{ $customer->created_at->format('d-m-Y') }}</td>
                </tr>
                <tr>
                    <th class="table-secondary" scope="row">Data ostatniej aktualizacji:</th>
                    <td>{{ $customer->updated_at->format('d-m-Y') }}</td>
                </tr>
                </tbody>
            </table>
        </li>
        <li>
            <table class="table table-bordered table-hover table-flat table-striped table-toggle" cellspacing="0" width="100%">
                <thead>
                    <tr class="thead-dark">
                        <th colspan="2">
                            Informacje o zakupie
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th class="table-secondary" scope="row">
                            {!! Form::label('mode_purchase_id', "Tryb zakupu:") !!}
                        </th>
                        <td>
                            <div class="form-flat{{ $errors->has('mode_purchase_id') ? ' has-error' : '' }}">
                                {!! Form::select(
                                        'mode_purchase_id',
                                        $modePurchase,
                                        $customer->purchase ? $customer->purchase->mode_purchase_id : null,
                                        ['class' => 'form-ghost']
                                    )
                                !!}

                                @if ($errors->has('mode_purchase_id'))
                                    <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('mode_purchase_id') }}</strong>
                                </span>
                                @endif

                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th class="table-secondary" scope="row">{!! Form::label('price', "Cena zakupu (zł):") !!}</th>
                        <td>
                            <div class="form-flat{{ $errors->has('price') ? ' has-error' : '' }}">
                                {!! Form::text(
                                        'price',
                                        $customer->purchase ? number_format($customer->purchase->price, 2, ',', ' ') : null,
                                        [
                                            'class' => 'form-ghost number'
                                        ]
                                    )
                                !!}

                                @if ($errors->has('price'))
                                    <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('price') }}</strong>
                                </span>
                                @endif

                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </li>
        <li>
            <table class="table table-bordered table-hover table-flat table-striped table-toggle" cellspacing="0" width="100%">
                <thead>
                <tr class="thead-dark">
                    <th colspan="2">
                        Informacje o nieruchomości
                    </th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th class="table-secondary" scope="row">{!! Form::label('garage_id', "Garaż:") !!}</th>
                    <td>
                        <div class="form-flat{{ $errors->has('garage_id') ? ' has-error' : '' }}">
                            {!! Form::select(
                                    'garage_id',
                                    $garage,
                                    $customer->property ? $customer->property->garage_id : null,
                                    [
                                        'class' => 'form-ghost',
                                        'placeholder' => 'Wybierz ...'
                                    ]
                                )
                            !!}

                            @if ($errors->has('garage_id'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('garage_id') }}</strong>
                                </span>
                            @endif

                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="table-secondary" scope="row">
                        {!! Form::label('valuation_id', "Wycena nieruchomości:") !!}
                    </th>
                    <td>
                        <div class="form-flat{{ $errors->has('valuation_id') ? ' has-error' : '' }}">
                            {!! Form::select(
                                    'valuation_id',
                                    $valuations,
                                    $customer->property ? $customer->property->valuation_id : null,
                                    [
                                        'class' => 'form-ghost',
                                        'placeholder' => 'Wybierz ...'
                                    ]
                                )
                            !!}

                            @if ($errors->has('valuation_id'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('valuation_id') }}</strong>
                                </span>
                            @endif

                        </div>

                        <table class="mt-3 table-file" cellspacing="0" width="100%">
                            <thead>
                                <tr class="thead-dark">
                                    <th colspan="2">Pliki</th>
                                </tr>
                            </thead>
                            <tbody id="addNewFile">

                                <tr>
                                    <td colspan="2">

                                        <div class="form-flat{{ $errors->has('file_valuation') ? ' has-error' : '' }}">

                                            {!! Form::file('file_valuation[]', ['multiple', 'id' => 'uploadFilesCustomer']) !!}

                                            <div class="progress" style="display: none;">
                                                <div id="progressBar" class="progress-bar progress-bar-striped progress-bar-animated"
                                                     role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"
                                                     style="width: 0%">0%
                                                </div>
                                            </div>

                                            @if ($errors->has('file_valuation'))
                                                <span class="help-block">
                                                    <strong class="text-danger">{{ $errors->first('file_valuation') }}</strong>
                                                </span>
                                            @endif

                                        </div>

                                    </td>
                                </tr>

                                @if (!empty($fileValuation))

                                    @foreach ($fileValuation as $file)

                                        <tr class="row-{{ $file->id }}">
                                            <td>
                                                <a href="{{ url('storage/users/' . $customer->id . '/wycena_nieruchomosci') . '/' . $file->name }}" target="_blank">
                                                    {{ $file->name }}
                                                </a>
                                            </td>
                                            <td>
                                                <a class="btn-flat-black pull-right delete-file"
                                                   data-fileid="{{ $file->id }}">
                                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                                </a>
                                            </td>
                                        </tr>

                                    @endforeach

                                @endif

                            </tbody>
                        </table>

                    </td>
                </tr>
                <tr>
                    <th class="table-secondary" scope="row">
                        {!! Form::label('land_register', "Księga wieczysta",
                                ['for' => 'activeCheckLandRegister']) !!}
                    </th>
                    <td>
                        {!! Form::checkbox('land_register', true, ['id' => 'activeCheckLandRegister']) !!}
                    </td>
                </tr>
                <tr>
                    <th class="table-secondary" scope="row">
                        {!! Form::label('plot_number', "Numer działki:") !!}
                    </th>
                    <td>
                        <div class="form-flat{{ $errors->has('plot_number') ? ' has-error' : '' }}">
                            {!! Form::text(
                                    'plot_number',
                                    $customer->property ?
                                    $customer->property->plot_number
                                    : null,
                                    [
                                        'class' => 'form-ghost',
                                        'placeholder' => 'Numer działki'
                                    ]
                                )
                            !!}

                            @if ($errors->has('plot_number'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('plot_number') }}</strong>
                                </span>
                            @endif

                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="table-secondary" scope="row">
                        {!! Form::label('repairs_price', "Koszt wykończenia/remontu (zł):") !!}
                    </th>
                    <td>
                        <div class="form-flat{{ $errors->has('repairs_price') ? ' has-error' : '' }}">
                            {!! Form::text(
                                    'repairs_price',
                                    $customer->property ?
                                    number_format($customer->property->repairs_price, 2, ',', ' ')
                                    : null,
                                    [
                                        'class' => 'form-ghost number'
                                    ]
                                )
                            !!}

                            @if ($errors->has('repairs_price'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('repairs_price') }}</strong>
                                </span>
                            @endif

                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </li>
        <li>
            <table class="table table-bordered table-hover table-flat table-striped table-toggle" cellspacing="0" width="100%">
                <thead>
                    <tr class="thead-dark">
                        <th colspan="2">
                            Informacje o kredycie
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th class="table-secondary" scope="row">
                            {!! Form::label('purpose_id', "Cel kredytu:") !!}
                        </th>
                        <td>
                            <div class="form-flat{{ $errors->has('purpose_id') ? ' has-error' : '' }}">
                                {!! Form::select(
                                        'purpose_id',
                                        $purposes,
                                        $customer->credit ? $customer->credit->purpose_id : null,
                                        ['class' => 'form-ghost']
                                    )
                                !!}

                                @if ($errors->has('purpose_id'))
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('purpose_id') }}</strong>
                                    </span>
                                @endif

                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th class="table-secondary" scope="row">
                            {!! Form::label('amount', "Kwota kredytu (zł):") !!}
                        </th>
                        <td>
                            <div class="form-flat{{ $errors->has('amount') ? ' has-error' : '' }}">
                                {!! Form::text(
                                        'amount',
                                        $customer->credit ?
                                        number_format($customer->credit->amount, 2, ',', ' ')
                                        : null,
                                        [
                                            'class' => 'form-ghost number'
                                        ]
                                    )
                                !!}

                                @if ($errors->has('amount'))
                                    <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('amount') }}</strong>
                                </span>
                                @endif

                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th class="table-secondary" scope="row">
                            {!! Form::label('amount_own', "Wkład własny (zł):") !!}
                        </th>
                        <td>
                            <div class="form-flat{{ $errors->has('amount_own') ? ' has-error' : '' }}">
                                {!! Form::text(
                                        'amount_own',
                                        $customer->credit ?
                                        number_format($customer->credit->amount_own, 2, ',', ' ')
                                        : null,
                                        [
                                            'class' => 'form-ghost number'
                                        ]
                                    )
                                !!}

                                @if ($errors->has('amount_own'))
                                    <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('amount_own') }}</strong>
                                </span>
                                @endif

                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th class="table-secondary" scope="row">
                            {!! Form::label('description', "Opis kredytu:") !!}
                        </th>
                        <td>
                            <div class="form-flat{{ $errors->has('description') ? ' has-error' : '' }}">
                                {!! Form::textarea(
                                        'description',
                                        $customer->credit ? $customer->credit->description : null,
                                        ['class' => 'form-ghost']
                                    )
                                !!}

                                @if ($errors->has('description'))
                                    <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('description') }}</strong>
                                </span>
                                @endif

                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th class="table-secondary" scope="row">
                            {!! Form::label('property_description', "Informacja o celu kredytu:") !!}
                        </th>
                        <td>
                            <div class="form-flat{{ $errors->has('property_description') ? ' has-error' : '' }}">
                                {!! Form::textarea(
                                        'property_description',
                                        $customer->credit ? $customer->credit->property_description : null,
                                        ['class' => 'form-ghost']
                                    )
                                !!}

                                @if ($errors->has('property_description'))
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('property_description') }}</strong>
                                    </span>
                                @endif

                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th class="table-secondary" scope="row">
                            {!! Form::label('payment_date', "Data wypłaty kredytu:") !!}
                        </th>
                        <td>
                            <div class="form-flat{{ $errors->has('payment_date') ? ' has-error' : '' }}">
                                {!! Form::text(
                                        'payment_date',
                                        $customer->credit
                                            ? date('d-m-Y', strtotime($customer->credit->payment_date))
                                            : null,
                                        ['class' => 'form-ghost datepicker']
                                    )
                                !!}

                                @if ($errors->has('payment_date'))
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('payment_date') }}</strong>
                                    </span>
                                @endif

                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th class="table-secondary" scope="row">
                            {!! Form::label('loan_period', "Okres kredytowania:") !!}
                        </th>
                        <td>
                            <div class="form-flat{{ $errors->has('loan_period') ? ' has-error' : '' }}">
                                {!! Form::text(
                                        'loan_period',
                                        $customer->credit
                                            ? $customer->credit->loan_period
                                            : null,
                                        ['class' => 'form-ghost']
                                    )
                                !!}

                                @if ($errors->has('loan_period'))
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('loan_period') }}</strong>
                                    </span>
                                @endif

                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </li>
    </ul>

    {!! Form::close() !!}

