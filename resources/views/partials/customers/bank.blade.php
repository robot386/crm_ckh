<h5 class="pull-left head-toggle-bank">Preferowane banki</h5>

<div class="form-group text-right">

    {!! Form::button('Dodaj kolejny bank', ['id' => 'addNewBank',
        'class' => 'btn btn-outline-primary', 'data-toggle' => 'modal', 'data-target' => '#BankModal']) !!}

</div>

<div class="element-toggle-bank">
    <table class="table table-bordered table-hover table-flat table-striped" cellspacing="0" width="100%">
        <thead>
            <tr class="thead-dark">
                <th>Nazwa</th>
                <th colspan="2">Priorytet</th>
            </tr>
        </thead>
        <tbody id="selectBankList">

        @if (isset($customer) && count($customer->process))

            @foreach ($customer->getDistinctProcesses($customer->process) as $item)

                <tr class="row-{{ $item->bank_id }}">
                    <td>
                        <div class="form-flat{{ $errors->has('name') ? ' has-error' : '' }}">
                            {!! Form::text('name', $item->bank->name, ['class' => 'form-ghost']) !!}

                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('name') }}</strong>
                                </span>
                            @endif

                        </div>
                    </td>
                    <td>
                        <div class="form-flat{{ $errors->has('bank_prefer') ? ' has-error' : '' }}">
                            {!! Form::number('bank_prefer', $item->bank_prefer,
                                ['class' => 'form-ghost', 'data-prefer' => $item->bank_id]) !!}

                            @if ($errors->has('bank_prefer'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('bank_prefer') }}</strong>
                                </span>
                            @endif

                        </div>
                    </td>
                    <td style="width: 1rem;">

                        {!! Form::button('<i class="fa fa-trash" aria-hidden="true"></i>',
                            ['data-id' => $item->bank_id, 'data-customer_id' => $customer->id, 'type' => 'button',
                            'class' => 'btn-flat-black pull-right bank-delete']) !!}

                    </td>
                </tr>

            @endforeach

        @endif

        </tbody>
    </table>
</div>





