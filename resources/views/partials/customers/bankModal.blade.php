<div class="modal fade" id="BankModal" tabindex="-1" role="dialog" aria-labelledby="BankModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="BankModalLabel">Dodaj nowy bank</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="alert-bank"></div>

            {!! Form::open(['id' => 'addNewBankForm', 'url' => 'customer/process/add/banks']) !!}
            {!! Form::hidden('customer_id', $customer->id, ['class' => 'customer_id']) !!}

                <div class="modal-body">

                    <ul class="list-group" id="selectNewBank">

                        @foreach ($newBanks as $id => $bank)

                            <li class="list-group-item" style="padding: 0; border: none;">
                                {!! Form::checkbox('id-' . $id, $id) !!} {!! Form::label('id-' . $id, $bank) !!}
                            </li>

                        @endforeach

                    </ul>

                </div>
                <div class="modal-footer">
                    {!! Form::button('Zamknij', ['class'=>'btn btn-outline-primary', 'data-dismiss' => 'modal']) !!}
                </div>

            {!! Form::close() !!}

        </div>
    </div>
</div>