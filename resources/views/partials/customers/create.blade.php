
{!! Form::open(['id' => 'createCustomerForm', 'route' => 'customer.store', 'files' => true]) !!}

    {!! Form::hidden('id', $customer->id ?? 0, ['class' => 'customer_id']) !!}
    {!! Form::hidden('property_id', $customer->property->id ?? 0, ['class' => 'property_id']) !!}

    <h5 class="pull-left">Nowy kredytobiorca</h5>

    <div class="form-group text-right">
        {!! link_to(URL::previous(), 'Powrót', ['class' => 'btn btn-outline-primary']) !!}
    </div>

    @include('partials.status')

    <table class="table table-bordered table-hover table-flat table-striped" cellspacing="0" width="100%">
        <thead>
            <tr class="thead-dark">
                <th colspan="2">
                    Informacje podstawowe
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th class="table-secondary" scope="row">{!! Form::label('name', "Nazwisko i imię:") !!}
                    <span class="text-danger pull-right">*</span>
                </th>
                <td>
                    <div class="form-flat{{ $errors->has('name') ? ' has-error' : '' }}">
                        {!! Form::text('name', null,
                            ['class' => 'form-ghost', 'placeholder' => 'Nazwisko i imię']) !!}

                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong class="text-danger">{{ $errors->first('name') }}</strong>
                            </span>
                        @endif

                    </div>
                </td>
            </tr>
            <tr>
                <th class="table-secondary" scope="row">{!! Form::label('phone', "Telefon:") !!}</th>
                <td>
                    <div class="form-flat{{ $errors->has('phone') ? ' has-error' : '' }}">
                        {!! Form::text('phone', null,
                            ['class' => 'form-ghost', 'placeholder' => 'Telefon']) !!}

                        @if ($errors->has('phone'))
                            <span class="help-block">
                                <strong class="text-danger">{{ $errors->first('phone') }}</strong>
                            </span>
                        @endif

                    </div>
                </td>
            </tr>
            <tr>
                <th class="table-secondary" scope="row">{!! Form::label('email', "E-mail:") !!}
                    <span class="text-danger pull-right">*</span>
                </th>
                <td>
                    <div class="form-flat{{ $errors->has('email') ? ' has-error' : '' }}">
                        {!! Form::text('email', null,
                            ['class' => 'form-ghost', 'placeholder' => 'E-mail']) !!}

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong class="text-danger">{{ $errors->first('email') }}</strong>
                            </span>
                        @endif

                    </div>
                </td>
            </tr>
            <tr>
                <th class="table-secondary" scope="row">{!! Form::label('address', "Adres:") !!}</th>
                <td>
                    <div class="form-flat{{ $errors->has('address') ? ' has-error' : '' }}">
                        {!! Form::text('address', null,
                            ['class' => 'form-ghost', 'placeholder' => 'Adres']) !!}

                        @if ($errors->has('address'))
                            <span class="help-block">
                                <strong class="text-danger">{{ $errors->first('address') }}</strong>
                            </span>
                        @endif

                    </div>
                </td>
            </tr>
            <tr>
                <th class="table-secondary" scope="row">
                    {!! Form::label('next_contract_date', "Data następnej umowy:") !!}
                </th>
                <td>
                    <div class="form-flat{{ $errors->has('next_contract_date') ? ' has-error' : '' }}">
                        {!! Form::text('next_contract_date', null,
                        ['class' => 'form-ghost datepicker', 'placeholder' => 'Data następnej umowy']) !!}

                        @if ($errors->has('next_contract_date'))
                            <span class="help-block">
                                <strong class="text-danger">{{ $errors->first('next_contract_date') }}</strong>
                            </span>
                        @endif

                    </div>
                </td>
            </tr>
            <tr>
                <th class="table-secondary" scope="row">
                    {!! Form::label('source_customer_description', "Źródło pozyskania klienta:") !!}
                </th>
                <td>
                    <div class="form-flat{{ $errors->has('source_customer_description') ? ' has-error' : '' }}">
                        {!! Form::textarea('source_customer_description', null,
                            ['class' => 'form-ghost', 'placeholder' => 'Krótki opis']) !!}

                        @if ($errors->has('source_customer_description'))
                            <span class="help-block">
                                <strong class="text-danger">{{ $errors->first('source_customer_description') }}</strong>
                            </span>
                        @endif

                    </div>
                </td>
            </tr>
        </tbody>
    </table>

    <table class="table table-bordered table-hover table-flat table-striped" cellspacing="0" width="100%">
        <thead>
            <tr class="thead-dark">
                <th colspan="2">
                    Informacje o zakupie
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th class="table-secondary" scope="row">
                    {!! Form::label('mode_purchase_id', "Tryb zakupu:") !!}
                </th>
                <td>
                    <div class="form-flat{{ $errors->has('mode_purchase_id') ? ' has-error' : '' }}">
                        {!! Form::select('mode_purchase_id', $modePurchase, null, ['class' => 'form-ghost']) !!}

                        @if ($errors->has('mode_purchase_id'))
                            <span class="help-block">
                                <strong class="text-danger">{{ $errors->first('mode_purchase_id') }}</strong>
                            </span>
                        @endif

                    </div>
                </td>
            </tr>
            <tr>
            <th class="table-secondary" scope="row">{!! Form::label('price', "Cena zakupu (zł):") !!}</th>
            <td>
                <div class="form-flat{{ $errors->has('price') ? ' has-error' : '' }}">
                    {!! Form::text('price', null,
                        ['class' => 'form-ghost number', 'placeholder' => 'Cena zakupu']) !!}

                    @if ($errors->has('price'))
                        <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('price') }}</strong>
                        </span>
                    @endif

                </div>
            </td>
        </tr>
        </tbody>
    </table>

    <table class="table table-bordered table-hover table-flat table-striped" cellspacing="0" width="100%">
        <thead>
            <tr class="thead-dark">
                <th colspan="2">
                    Informacje o nieruchomości
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th class="table-secondary" scope="row">{!! Form::label('garage_id', "Garaż:") !!}</th>
                <td>
                    <div class="form-flat{{ $errors->has('garage_id') ? ' has-error' : '' }}">
                        {!! Form::select('garage_id', $garage, null,
                            ['class' => 'form-ghost', 'placeholder' => 'Wybierz ...']) !!}

                        @if ($errors->has('garage_id'))
                            <span class="help-block">
                                <strong class="text-danger">{{ $errors->first('garage_id') }}</strong>
                            </span>
                        @endif

                    </div>
                </td>
            </tr>
            <tr>
                <th class="table-secondary" scope="row">
                    {!! Form::label('valuation_id', "Wycena nieruchomości:") !!}
                </th>
                <td>
                    <div class="form-flat{{ $errors->has('valuation_id') ? ' has-error' : '' }}">
                        {!! Form::select('valuation_id', $valuations, null,
                                ['class' => 'form-ghost', 'placeholder' => 'Wybierz ...']) !!}

                        @if ($errors->has('valuation_id'))
                            <span class="help-block">
                                <strong class="text-danger">{{ $errors->first('valuation_id') }}</strong>
                            </span>
                        @endif

                    </div>

                    <table class="mt-3 table-file" cellspacing="0" width="100%">
                        <thead>
                            <tr class="thead-dark">
                                <th colspan="2">
                                    Pliki
                                </th>
                            </tr>
                        </thead>
                        <tbody id="addNewFile">
                            <tr>
                                <td colspan="2">

                                    <div class="form-flat{{ $errors->has('file_valuation') ? ' has-error' : '' }}">

                                        {!! Form::file('file_valuation[]', ['multiple', 'id' => 'uploadFilesCustomer']) !!}

                                        <div class="progress" style="display: none;">
                                            <div id="progressBar" class="progress-bar progress-bar-striped progress-bar-animated"
                                                 role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"
                                                 style="width: 0%">0%
                                            </div>
                                        </div>

                                        @if ($errors->has('file_valuation'))
                                            <span class="help-block">
                                                <strong class="text-danger">{{ $errors->first('file_valuation') }}</strong>
                                            </span>
                                        @endif

                                    </div>

                                </td>
                            </tr>
                        </tbody>
                    </table>

                </td>
            </tr>
            <tr>
                <th class="table-secondary" scope="row">
                    {!! Form::label('land_register', "Księga wieczysta",
                            ['for' => 'activeCheckLandRegister']) !!}
                </th>
                <td>
                    {!! Form::checkbox('land_register', true, ['id' => 'activeCheckLandRegister']) !!}
                </td>
            </tr>
            <tr>
                <th class="table-secondary" scope="row">
                    {!! Form::label('plot_number', "Numer działki:") !!}
                </th>
                <td>
                    <div class="form-flat{{ $errors->has('plot_number') ? ' has-error' : '' }}">
                        {!! Form::text('plot_number', null,
                                ['class' => 'form-ghost', 'placeholder' => 'Numer działki']) !!}

                        @if ($errors->has('plot_number'))
                            <span class="help-block">
                                <strong class="text-danger">{{ $errors->first('plot_number') }}</strong>
                            </span>
                        @endif

                    </div>
                </td>
            </tr>
            <tr>
            <th class="table-secondary" scope="row">
                {!! Form::label('repairs_price', "Koszt wykończenia/remontu (zł):") !!}
            </th>
            <td>
                <div class="form-flat{{ $errors->has('repairs_price') ? ' has-error' : '' }}">
                    {!! Form::text('repairs_price', null,
                            ['class' => 'form-ghost number',
                                'placeholder' => 'Koszt wykończenia/remontu']) !!}

                    @if ($errors->has('repairs_price'))
                        <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('repairs_price') }}</strong>
                        </span>
                    @endif

                </div>
            </td>
        </tr>
        </tbody>
    </table>

    <table class="table table-bordered table-hover table-flat table-striped" cellspacing="0" width="100%">
        <thead>
            <tr class="thead-dark">
                <th colspan="2">
                    Informacje o kredycie
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th class="table-secondary" scope="row">
                    {!! Form::label('purpose_id', "Cel kredytu:") !!}
                </th>
                <td>
                    <div class="form-flat{{ $errors->has('purpose_id') ? ' has-error' : '' }}">
                        {!! Form::select('purpose_id', $purposes, null,
                            ['class' => 'form-ghost', 'placeholder' => 'Cel kredytu']) !!}

                        @if ($errors->has('purpose_id'))
                            <span class="help-block">
                                <strong class="text-danger">{{ $errors->first('purpose_id') }}</strong>
                            </span>
                        @endif

                    </div>
                </td>
            </tr>
            <tr>
                <th class="table-secondary" scope="row">
                    {!! Form::label('amount', "Kwota kredytu (zł):") !!}
                </th>
                <td>
                    <div class="form-flat{{ $errors->has('amount') ? ' has-error' : '' }}">
                        {!! Form::text('amount', null,
                            ['class' => 'form-ghost number',
                                'placeholder' => 'Kwota kredytu']) !!}

                        @if ($errors->has('amount'))
                            <span class="help-block">
                                <strong class="text-danger">{{ $errors->first('amount') }}</strong>
                            </span>
                        @endif

                    </div>
                </td>
            </tr>
            <tr>
                <th class="table-secondary" scope="row">
                    {!! Form::label('amount_own', "Wkład własny (zł):") !!}
                </th>
                <td>
                    <div class="form-flat{{ $errors->has('amount_own') ? ' has-error' : '' }}">
                        {!! Form::text('amount_own', null,
                            ['class' => 'form-ghost number',
                                'placeholder' => 'Wkład własny']) !!}

                        @if ($errors->has('amount_own'))
                            <span class="help-block">
                                <strong class="text-danger">{{ $errors->first('amount_own') }}</strong>
                            </span>
                        @endif

                    </div>
                </td>
            </tr>
            <tr>
                <th class="table-secondary" scope="row">
                    {!! Form::label('description', "Opis kredytu:") !!}
                </th>
                <td>
                    <div class="form-flat{{ $errors->has('description') ? ' has-error' : '' }}">
                        {!! Form::textarea('description', null,
                            ['class' => 'form-ghost', 'placeholder' => 'Krótki opis']) !!}

                        @if ($errors->has('description'))
                            <span class="help-block">
                                <strong class="text-danger">{{ $errors->first('description') }}</strong>
                            </span>
                        @endif

                    </div>
                </td>
            </tr>
            <tr>
                <th class="table-secondary" scope="row">
                    {!! Form::label('property_description', "Informacja o celu kredytu:") !!}
                </th>
                <td>
                    <div class="form-flat{{ $errors->has('property_description') ? ' has-error' : '' }}">
                        {!! Form::textarea('property_description', null,
                                ['class' => 'form-ghost', 'placeholder' => 'Krótki opis']) !!}

                        @if ($errors->has('property_description'))
                            <span class="help-block">
                                <strong class="text-danger">{{ $errors->first('property_description') }}</strong>
                            </span>
                        @endif

                    </div>
                </td>
            </tr>
            <tr>
                <th class="table-secondary" scope="row">
                    {!! Form::label('payment_date', "Data wypłaty kredytu:") !!}
                </th>
                <td>
                    <div class="form-flat{{ $errors->has('payment_date') ? ' has-error' : '' }}">
                        {!! Form::text('payment_date', null,
                                ['class' => 'form-ghost datepicker', 'placeholder' => 'Data wypłaty kredytu']) !!}

                        @if ($errors->has('payment_date'))
                            <span class="help-block">
                                <strong class="text-danger">{{ $errors->first('payment_date') }}</strong>
                            </span>
                        @endif

                    </div>
                </td>
            </tr>
            <tr>
                <th class="table-secondary" scope="row">
                    {!! Form::label('loan_period', "Okres kredytowania:") !!}
                </th>
                <td>
                    <div class="form-flat{{ $errors->has('loan_period') ? ' has-error' : '' }}">
                        {!! Form::text('loan_period', null,
                            ['class' => 'form-ghost', 'placeholder' => 'Okres kredytowania']) !!}

                        @if ($errors->has('loan_period'))
                            <span class="help-block">
                                <strong class="text-danger">{{ $errors->first('loan_period') }}</strong>
                            </span>
                        @endif

                    </div>
                </td>
            </tr>
        </tbody>
    </table>

    <div class="form-group text-right">
        {!! link_to(URL::previous(), 'Powrót', ['class' => 'btn btn-outline-primary']) !!}
    </div>

{!! Form::close() !!}
