
<div class="time-element text-center">
    <span class="agreement-time m-1 {{ $colorAgreementClass }}">
        <b>
            {{ $agreementTime }}
        </b>
    </span>
    <span class="paid-time m-1 {{ $colorClass }}">
        <b>
            {{ $paidTime }}
        </b>
    </span>
</div>

<div class="clearfix"></div>

<div class="load-carousel">

@foreach ($customer->getDistinctProcesses($processes) as $process)

    <div class="element-carousel container-carousel-{{ $process->bank_id }}">

        <h5 class="pull-left">

            <span class="bank-prefer-{{ $process->bank_id }}">{{ $process->bank_prefer ? $process->bank_prefer . ' - ' : ''}}</span>

            <a href="{{ url('customer/process/run/browser') . '/' . $process->bank->browser->name . '/' . base64_encode($process->bank->url) }}"
               title="{{ $process->bank->name }}">
                <img class="logo-size" src="{{ asset('storage/banks/logo') }}/{{ $process->bank->logo }}" alt="{{ $process->bank->name }}">
            </a>

            @if ($process->bank->contact_file)

                <a target="_blank" href="{{ asset('storage/banks/contact') }}/{{ $process->bank->contact_file }}">
                    <i class="fa fa-phone m-2"></i>
                </a>

            @else

                <span class="file_contact_link-{{ $process->bank_id }} m-1">

                    {!! Form::open(['url' => 'customer/process/file/contact', 'files' => true,
                        'id' => 'addContactFile-' . $process->bank_id, 'class' => 'file_contact_form pull-right']) !!}

                    {!! Form::hidden('bank_id', $process->bank_id, ['id' => 'bank_id']) !!}

                    <label for="file_contact">
                        <i class="fa fa-phone" title="Brak pliku z danymi kontaktowymi" style="color: silver;"></i>
                    </label>

                    {!! Form::file('file_contact', ['id' => 'file_contact']) !!}

                    {!! Form::close() !!}

                </span>

            @endif

        </h5>

        <div class="form-group text-right">

            @if (!$process->status)

                {!! link_to(url('customer/process/set/status') . '/' . $process->customer_id . '/' . $process->bank_id,
                    'Przywróć', ['class' => 'btn btn-outline-primary']) !!}

                {!! Form::open(['url' => 'customer/process/delete/' . $process->customer_id . '/' . $process->bank_id,
                    'onsubmit' => 'return confirmDelete("bank");', 'class' => 'd-inline']) !!}

                {!! Form::hidden('_method', 'DELETE') !!}
                {!! Form::button('Usuń', ['type' => 'submit', 'class' => 'btn btn-outline-primary']) !!}

                {!! Form::close() !!}

            @else

                {!! link_to(url('customer/process/edit') . '/' . $process->customer_id . '/' . $process->bank_id,
                    'Edytuj procesy', ['class' => 'btn btn-outline-primary']) !!}

                {!! link_to(url('customer/process/unset/status') . '/' . $process->customer_id . '/' . $process->bank_id,
                    'Wyłącz', ['class' => 'btn btn-outline-primary']) !!}

            @endif

        </div>

        <div class="clearfix"></div>

        <div class="owl-carousel owl-theme">

        @foreach ($processes as $i => $item)

            @if ($item->bank_id == $process->bank_id)

                <div class="item" data-hash="{{ $item->active ? 'activeProcess' : '' }}">
                <div class="p-1">

                    <a class="wheel-active-{{ $process->bank_id }}"
                       data-process="{{ $item->id }}"
                       data-cid="{{ $process->customer_id }}"
                       data-bid="{{ $process->bank_id }}">

                        <i class="fa {{ $item->active ? 'fa-check-square' : 'fa-square' }} check-active"></i>
                    </a>

                    <i class="fa fa-paint-brush color-process" data-id="{{ $item->id }}"></i>

                    <div class="mx-auto wheel-big" id="color-{{ $item->id }}"
                         style="{{ $item->color ? 'background-color: ' . $item->color . ';' : '' }}">
                        <div class="mx-2">{{ $item->name }}</div>
                    </div>

                </div>
                <div class="p-1">

                    {!! Form::text('date', $item->date ? date('d-m-Y', strtotime($item->date)) : null,
                        ['data-id' => $item->id, 'class' => 'form-control form-control-sm form-date datepicker']) !!}

                </div>
                <div class="p-1">

                    <table class="table table-bordered table-hover table-sm no-space" cellspacing="0">
                        <thead class="{{ !$item->status ? 'bg-secondary text-white' : '' }}">
                        <tr>
                            <th colspan="2" scope="col">

                                <a class="btn-flat pull-left"
                                   data-toggle="modal"
                                   data-target="#NoteModal"
                                   data-whatever="Notatka - {{ $item->name }}"
                                   data-cpid="{{ $item->id }}"
                                   title="Dodaj notatkę">
                                    <i class="fa fa-wpforms"></i>
                                </a>

                            </th>
                        </tr>
                        </thead>
                        <tbody class="note-{{ $item->id }}" id="bodyNote">

                        @if (count($item->notes))

                            @foreach ($item->notes as $note)

                                <tr class="row-{{ $note->id }}">
                                    <td class="with-process">

                                        <a class="btn-flat-black pull-left"
                                           data-toggle="modal"
                                           data-target="#NoteModal"
                                           data-id="{{ $note->id }}"
                                           data-cpid="{{ $note->customer_process_id }}"
                                           data-description="{{ $note->description }}"
                                           title="{{ $note->description }}">
                                            {{ $note->description }}
                                        </a>

                                    </td>
                                    <td>

                                        {!! Form::button('<i class="fa fa-trash" aria-hidden="true"></i>',
                                            ['data-id' => $note->id, 'type' => 'button',
                                             'class' => 'btn-flat-black pull-right note-delete']) !!}

                                    </td>
                                </tr>

                            @endforeach

                        @endif

                        <tr class="row-"></tr>

                        </tbody>
                    </table>

                </div>
                <div class="p-1">

                    <style>
                        .add-file-process {
                            position: relative;
                            overflow: hidden;
                        }
                        .add-file-process input[type=file] {
                            position: absolute;
                            top: 0;
                            right: 0;
                            min-width: 100%;
                            min-height: 100%;
                            font-size: 100px;
                            text-align: right;
                            filter: alpha(opacity=0);
                            opacity: 0;
                            outline: none;
                            background: white;
                            cursor: inherit;
                            display: block;
                        }
                    </style>

                    <table class="table table-bordered table-hover table-sm no-space" cellspacing="0">
                        <thead class="{{ !$item->status ? 'bg-secondary text-white' : '' }}">
                            <tr>
                                <th colspan="2" scope="col">

                                    <a class="btn-flat pull-left" title="Dodaj plik">
                                        <i class="fa fa-file add-file-process"
                                           data-cpid="{{ $item->id }}"
                                           data-cid="{{ $process->customer_id }}"
                                           data-bid="{{ $process->bank_id }}">
                                            <input name="file" type="file">
                                        </i>
                                    </a>

                                </th>
                            </tr>
                        </thead>
                        <tbody class="file-{{ $item->id }}" id="bodyFile">

                            @if (count($item->files))

                                @foreach ($item->files as $file)

                                    <tr class="row-{{ $file->id }}">
                                        <td class="with-process">
                                            <a href="{{ url('storage/users/' . $process->customer_id . '/' .
                                                $process->bank_id . '/' . $item->id) . '/' . $file->name }}" target="_blank">
                                                {{ $file->name ?: null }}
                                            </a>
                                        </td>
                                        <td>

                                            {!! Form::button('<i class="fa fa-trash" aria-hidden="true"></i>',
                                                ['data-id' => $file->id, 'type' => 'button',
                                                 'class' => 'btn-flat-black pull-right file-delete']) !!}

                                        </td>
                                    </tr>

                                @endforeach

                            @endif

                        </tbody>
                    </table>

                </div>
            </div>

            @endif

        @endforeach

    </div>

    </div>

@endforeach

</div>