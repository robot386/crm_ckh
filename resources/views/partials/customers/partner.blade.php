<h5 class="pull-left head-toggle-partner">Współkredytobiorcy</h5>

<div class="form-group text-right">

    {!! Form::button('Dodaj współkredytobiorcę', ['id' => 'addCreditPartner', 'class' => 'btn btn-outline-primary']) !!}

</div>

<ul id="tableCreditPartner" class="element-toggle-partner">

    <li class="credit-partner"></li>

    @if (count($partners))

        <li>

            @foreach($partners as $partner)

                <div class="row-{{ $partner->id }}" id="partnerCredit-{{ $partner->id }}">

                    {!! Form::model($partners, ['id' => 'addCreditPartnerForm-' .$partner->id, 'method' => 'POST',
                        'action' => 'CustomersController@addCreditPartner']) !!}

                    {!! Form::hidden('customer_id', $customer->id) !!}
                    {!! Form::hidden('id', $partner->id, ['id' => 'partnerId-' . $partner->id, 'class' => 'exist']) !!}

                    <table class="table table-bordered table-hover table-flat table-striped table-toggle" cellspacing="0" width="100%">
                        <thead>
                            <tr class="thead-dark">
                                <th class="partner-name-{{ $partner->id }}">
                                    {{ $partner->name }}
                                </th>
                                <th>
                                    <a data-partner="{{ $partner->id }}" class="btn-flat pull-right del-partner"><i class="fa fa-remove"></i></a>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th class="table-secondary" scope="row">{!! Form::label('name', "Nazwisko i imię:") !!}
                                    <span class="text-danger pull-right">*</span>
                                </th>
                                <td>
                                    <div class="form-flat{{ $errors->has('name') ? ' has-error' : '' }}">
                                        {!! Form::text('name', $partner->name,
                                            ['class' => 'form-ghost name-partner', 'data-partner_id' => $partner->id ]) !!}

                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong class="text-danger">{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif

                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th class="table-secondary" scope="row">{!! Form::label('phone', "Telefon:") !!}</th>
                                <td>
                                    <div class="form-flat{{ $errors->has('phone') ? ' has-error' : '' }}">
                                        {!! Form::text('phone', $partner->phone, ['class' => 'form-ghost']) !!}

                                        @if ($errors->has('phone'))
                                            <span class="help-block">
                                                <strong class="text-danger">{{ $errors->first('phone') }}</strong>
                                            </span>
                                        @endif

                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th class="table-secondary" scope="row">{!! Form::label('email', "E-mail:") !!}
                                    <span class="text-danger pull-right">*</span>
                                </th>
                                <td>
                                    <div class="form-flat{{ $errors->has('email') ? ' has-error' : '' }}">
                                        {!! Form::text('email', $partner->email, ['class' => 'form-ghost']) !!}

                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong class="text-danger">{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif

                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th class="table-secondary" scope="row">{!! Form::label('address', "Adres:") !!}</th>
                                <td>
                                    <div class="form-flat{{ $errors->has('address') ? ' has-error' : '' }}">
                                        {!! Form::text('address', $partner->address, ['class' => 'form-ghost']) !!}

                                        @if ($errors->has('address'))
                                            <span class="help-block">
                                                <strong class="text-danger">{{ $errors->first('address') }}</strong>
                                            </span>
                                        @endif

                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th class="table-secondary" scope="row">Data dodania klienta:</th>
                                <td>{{ $partner->created_at->format('d-m-Y') }}</td>
                            </tr>
                            <tr>
                                <th class="table-secondary" scope="row">Data ostatniej aktualizacji:</th>
                                <td>{{ $partner->updated_at->format('d-m-Y') }}</td>
                            </tr>
                        </tbody>
                    </table>

                    {!! Form::close() !!}

                </div>

            @endforeach

        </li>

    @endif

</ul>



