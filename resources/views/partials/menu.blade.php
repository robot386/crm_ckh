<ul class="nav-bootom nav nav-pills nav-fill">
    <li class="nav-item">
        <a class="nav-link btn btn-outline-primary {{ Route::is('customer.create') ? 'active' : null }}"
           href="{{ route('customer.create') }}#newCustomerForm">NOWY KREDYTOBIORCA</a>
    </li>

    <li class="nav-item">
        <a class="nav-link btn btn-outline-primary {{ Route::is('banks') ? 'active' : null }}"
           href="{{ route('banks') }}">KONFIGURACJA</a>
    </li>

    <li class="nav-item">
        <a class="nav-link btn btn-outline-primary {{ Route::is('customers') ? 'active' : null }}"
           href="{{ route('customers') }}">AKTYWNI KREDYTOBIORCY</a>
    </li>

    <li class="nav-item">
        <a class="nav-link btn btn-outline-primary {{ Route::is('customers.archive') ? 'active' : null }}"
           href="{{ route('customers.archive') }}">ARCHIWUM</a>
    </li>
</ul>