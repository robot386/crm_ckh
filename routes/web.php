<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

Route::get('/', 'HomeController@login')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/register', 'HomeController@index')->name('home');

/**
 * Log Activity
 */
Route::get('log/activity', [
    'uses' => 'CustomerProcessController@logActivity',
    'as' => 'log.activity'
]);

/**
 * Customers
 */

Route::get('customers', [
    'uses' => 'CustomersController@index',
    'as' => 'customers'
]);

Route::get('customers/archive', [
    'uses' => 'CustomersController@archive',
    'as' => 'customers.archive'
]);

Route::get('customer/create', [
    'uses' => 'CustomersController@create',
    'as' => 'customer.create'
]);

Route::post('customer/store', [
    'uses' => 'CustomersController@store',
    'as' => 'customer.store'
]);

Route::get('customer/activate/{id}', [
    'uses' => 'CustomersController@activate',
    'as' => 'customer.activate'
]);

Route::get('customer/deactivate/{id}', [
    'uses' => 'CustomersController@deactivate',
    'as' => 'customer.deactivate'
]);

Route::delete('customer/delete/{id}', [
    'uses' => 'CustomersController@destroy',
    'as' => 'customer.delete'
]);

Route::patch('customer/update/{id}', [
    'uses' => 'CustomersController@update',
    'as' => 'customer.update'
]);

Route::patch('customer/update-bank/{id}', [
    'uses' => 'CustomersController@updateOnBank',
    'as' => 'customer.update_bank'
]);

Route::post('customer/partner/create', [
    'uses' => 'CustomersController@addCreditPartner',
    'as' => 'customer.partner.create'
]);

Route::delete('customer/partner/delete/{id}', [
    'uses' => 'CustomersController@deletePartner',
    'as' => 'customer.partner.delete'
]);

Route::delete('customer/file/delete/{id}', [
    'uses' => 'CustomersController@fileDelete',
    'as' => 'customer.file.delete'
]);

Route::post('customer/file/upload', [
    'uses' => 'CustomersController@uploadFiles',
    'as' => 'customer.file.upload'
]);

/**
 * CustomerProcess
 */

Route::get('customer/process/{cid}', [
    'uses' => 'CustomerProcessController@index',
    'as' => 'customer.process'
]);

Route::get('customer/process/set/status/{cid}/{bid}', [
    'uses' => 'CustomerProcessController@setStatus',
    'as' => 'customer.process.set.status'
]);

Route::get('customer/process/unset/status/{cid}/{bid}', [
    'uses' => 'CustomerProcessController@unsetStatus',
    'as' => 'customer.process.unset.status'
]);

Route::delete('customer/process/delete/{cid}/{bid}', [
    'uses' => 'CustomerProcessController@deleteBank',
    'as' => 'customer.process.delete'
]);

Route::post('customer/process/activate', [
    'uses' => 'CustomerProcessController@activate',
    'as' => 'customer.process.activate'
]);

Route::get('customer/process/edit/{cid}/{bid}', [
    'uses' => 'CustomerProcessController@edit',
    'as' => 'customer.process.edit'
]);

Route::delete('customer/process/delete/{id}', [
    'uses' => 'CustomerProcessController@destroy',
    'as' => 'customer.process.destroy'
]);

Route::post('customer/process/update', [
    'uses' => 'CustomerProcessController@update',
    'as' => 'customer.process.update'
]);

Route::post('customer/process/color', [
    'uses' => 'CustomerProcessController@setColor',
    'as' => 'customer.process.color'
]);

Route::post('customer/process/date', [
    'uses' => 'CustomerProcessController@setDate',
    'as' => 'customer.process.date'
]);

Route::post('customer/process/bank/prefer', [
    'uses' => 'CustomerProcessController@setBankPrefer',
    'as' => 'customer.process.bank.prefer'
]);

Route::delete('customer/process/bank/delete/{cid}/{bid}', [
    'uses' => 'CustomerProcessController@deleteBankFromlist',
    'as' => 'customer.process.bank.delete'
]);

Route::get('customer/process/run/browser/{browser_name}/{url}', [
    'uses' => 'CustomerProcessController@runBrowser',
    'as' => 'customer.process.run.browser'
]);

Route::post('customer/process/file/contact', [
    'uses' => 'CustomerProcessController@saveContactFile',
    'as' => 'customer.process.file.contact'
]);

/**
 * Configuration
 */

/**
 * Banks
 */

Route::get('configuration/banks', [
    'uses' => 'BanksController@index',
    'as' => 'banks'
]);

Route::get('configuration/bank/create', [
    'uses' => 'BanksController@create',
    'as' => 'bank.create'
]);

Route::post('configuration/bank/store', [
    'uses' => 'BanksController@store',
    'as' => 'bank.store'
]);

Route::get('configuration/bank/edit/{id}', [
    'uses' => 'BanksController@edit',
    'as' => 'bank.edit'
]);

Route::patch('configuration/bank/update/{id}', [
    'uses' => 'BanksController@update',
    'as' => 'bank.update'
]);

Route::delete('configuration/bank/delete/{id}', [
    'uses' => 'BanksController@destroy',
    'as' => 'bank.delete'
]);

/**
 * Bank process
 */

Route::get('configuration/bank/process', [
    'uses' => 'ProcessController@index',
    'as' => 'process'
]);

Route::get('configuration/bank/process/create', [
    'uses' => 'ProcessController@create',
    'as' => 'process.create'
]);

Route::post('configuration/bank/process/store', [
    'uses' => 'BanksController@store',
    'as' => 'process.store'
]);

/**
 * Purposes
 */

Route::get('configuration/purposes', [
    'uses' => 'PurposesController@index',
    'as' => 'purposes'
]);

Route::get('configuration/purpose/create', [
    'uses' => 'PurposesController@create',
    'as' => 'purpose.create'
]);

Route::post('configuration/purpose/store', [
    'uses' => 'PurposesController@store',
    'as' => 'purpose.store'
]);

Route::get('configuration/purpose/edit/{id}', [
    'uses' => 'PurposesController@edit',
    'as' => 'purpose.edit'
]);

Route::patch('configuration/purpose/update/{id}', [
    'uses' => 'PurposesController@update',
    'as' => 'purpose.update'
]);

Route::delete('configuration/purpose/delete/{id}', [
    'uses' => 'PurposesController@destroy',
    'as' => 'purpose.delete'
]);

/**
 *  Mode_purchases
 */

Route::get('configuration/mode_purchases', [
    'uses' => 'ModePurchasesController@index',
    'as' => 'purchases'
]);

Route::get('configuration/mode_purchase/create', [
    'uses' => 'ModePurchasesController@create',
    'as' => 'mode_purchase.create'
]);

Route::post('configuration/mode_purchase/store', [
    'uses' => 'ModePurchasesController@store',
    'as' => 'mode_purchase.store'
]);

Route::get('configuration/mode_purchase/edit/{id}', [
    'uses' => 'ModePurchasesController@edit',
    'as' => 'mode_purchase.edit'
]);

Route::patch('configuration/mode_purchase/update/{id}', [
    'uses' => 'ModePurchasesController@update',
    'as' => 'mode_purchase.update'
]);

Route::delete('configuration/mode_purchase/delete/{id}', [
    'uses' => 'ModePurchasesController@destroy',
    'as' => 'mode_purchase.delete'
]);

/**
 * Garages
 */

Route::get('configuration/garages', [
    'uses' => 'GaragesController@index',
    'as' => 'garages'
]);

Route::get('configuration/garage/create', [
    'uses' => 'GaragesController@create',
    'as' => 'garage.create'
]);

Route::post('configuration/garage/store', [
    'uses' => 'GaragesController@store',
    'as' => 'garage.store'
]);

Route::get('configuration/garage/edit/{id}', [
    'uses' => 'GaragesController@edit',
    'as' => 'garage.edit'
]);

Route::patch('configuration/garage/update/{id}', [
    'uses' => 'GaragesController@update',
    'as' => 'garage.update'
]);

Route::delete('configuration/garage/delete/{id}', [
    'uses' => 'GaragesController@destroy',
    'as' => 'garage.delete'
]);

/**
 * Valuations
 */

Route::get('configuration/valuations', [
    'uses' => 'ValuationsController@index',
    'as' => 'valuations'
]);

Route::get('configuration/valuation/create', [
    'uses' => 'ValuationsController@create',
    'as' => 'valuation.create'
]);

Route::post('configuration/valuation/store', [
    'uses' => 'ValuationsController@store',
    'as' => 'valuation.store'
]);

Route::get('configuration/valuation/edit/{id}', [
    'uses' => 'ValuationsController@edit',
    'as' => 'valuation.edit'
]);

Route::patch('configuration/valuation/update/{id}', [
    'uses' => 'ValuationsController@update',
    'as' => 'valuation.update'
]);

Route::delete('configuration/valuation/delete/{id}', [
    'uses' => 'ValuationsController@destroy',
    'as' => 'valuation.delete'
]);

/**
 *  Users
 */

Route::get('configuration/users', [
    'uses' => 'UsersController@index',
    'as' => 'users'
]);

Route::get('configuration/user/create', [
    'uses' => 'UsersController@create',
    'as' => 'user.create'
]);

Route::post('configuration/user/store', [
    'uses' => 'UsersController@store',
    'as' => 'user.store'
]);

Route::get('configuration/user/edit/{id}', [
    'uses' => 'UsersController@edit',
    'as' => 'user.edit'
]);

Route::patch('configuration/user/update/{id}', [
    'uses' => 'UsersController@update',
    'as' => 'user.update'
]);

Route::delete('configuration/user/delete/{id}', [
    'uses' => 'UsersController@destroy',
    'as' => 'user.delete'
]);

/**
 * Browsers
 */

Route::get('configuration/browsers', [
    'uses' => 'BrowsersController@index',
    'as' => 'browsers'
]);

Route::get('configuration/browser/create', [
    'uses' => 'BrowsersController@create',
    'as' => 'browser.create'
]);

Route::post('configuration/browser/store', [
    'uses' => 'BrowsersController@store',
    'as' => 'browser.store'
]);

Route::get('configuration/browser/edit/{id}', [
    'uses' => 'BrowsersController@edit',
    'as' => 'browser.edit'
]);

Route::patch('configuration/browser/update/{id}', [
    'uses' => 'BrowsersController@update',
    'as' => 'browser.update'
]);

Route::delete('configuration/browser/delete/{id}', [
    'uses' => 'BrowsersController@destroy',
    'as' => 'browser.delete'
]);

/**
 * Notes
 */

Route::post('customer/process/note/store', [
    'uses' => 'NotesController@store',
    'as' => 'note.store'
]);

Route::delete('customer/process/note/delete/{id}', [
    'uses' => 'NotesController@destroy',
    'as' => 'note.delete'
]);

/**
 * Files
 */

Route::post('customer/process/file/store', [
    'uses' => 'FilesController@store',
    'as' => 'file.store'
]);

Route::delete('customer/process/file/delete/{id}', [
    'uses' => 'FilesController@destroy',
    'as' => 'file.delete'
]);

/**
 * Tranche
 */

Route::post('customer/tranches/store', [
    'uses' => 'TrancheController@store',
    'as' => 'tranche.store'
]);

Route::get('customer/tranche/{id}', [
    'uses' => 'TrancheController@show',
    'as' => 'tranche'
]);

/**
 * Change payment date
 */

Route::post('customer/change/payment/{date}', [
    'uses' => 'CustomerProcessController@changePaymentDate',
    'as' => 'change.payment'
]);

Route::post('customer/change/agreement/{date}', [
    'uses' => 'CustomerProcessController@changeAgreementDate',
    'as' => 'change.agreement'
]);

/**
 * Add banks
 */

Route::post('customer/process/add/banks', [
    'uses' => 'CustomerProcessController@addNewBanks',
    'as' => 'add.banks'
]);




