<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Role();
        $role->name = 'Admin';
        $role->description = 'Administrator systemu';
        $role->save();

        $role = new Role();
        $role->name = 'Moderator';
        $role->description = 'Moderator systemu';
        $role->save();

        $role = new Role();
        $role->name = 'User';
        $role->description = 'Zwykły użytkownik np. klient';
        $role->save();
    }
}
