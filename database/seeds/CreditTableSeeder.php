<?php

use Illuminate\Database\Seeder;
use App\Credit;

class CreditTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('pl_PL');

        foreach (range(1, 50) as $index) {
            $credit = new Credit();
            $credit->customer_id = $index;
            $credit->purpose_id = $faker->numberBetween(1, 20);
            $credit->amount = $faker->randomNumber(6);
            $credit->amount_own = $faker->randomNumber(6);
            $credit->description = $faker->text;
            $credit->property_description = $faker->text;
            $credit->payment_date = $faker->dateTime;
            $credit->loan_period = $faker->dateTime;
            $credit->save();
        }
    }
}
