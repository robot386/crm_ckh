<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roleAdmin = Role::where('name', 'Admin')->first();
        $roleModerator = Role::where('name', 'Moderator')->first();
        $roleUser = Role::where('name', 'User')->first();

        $user = new User();
        $user->name = 'Admin';
        $user->email = 'admin@admin.pl';
        $user->password = bcrypt('admin12345');
        $user->save();
        $user->roles()->attach($roleAdmin);

        $user = new User();
        $user->name = 'Moderator';
        $user->email = 'moderator@moderator.pl';
        $user->password = bcrypt('moderator12345');
        $user->save();
        $user->roles()->attach($roleModerator);

        $user = new User();
        $user->name = 'User';
        $user->email = 'user@user.pl';
        $user->password = bcrypt('user12345');
        $user->save();
        $user->roles()->attach($roleUser);
    }
}
