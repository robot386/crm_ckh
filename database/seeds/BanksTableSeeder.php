<?php

use Illuminate\Database\Seeder;
use App\Bank;

class BanksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $lists = [
            0 => [
                'name' => [
                    'PKO Bank Polski',
                ],
                'logo' => [
                    'Logotyp_PKO_BP.svg',
                ],
                'url' => [
                    'https://www.pkobp.pl/'
                ],
                'contact_file' => [
                    ''
                ]
            ],
            1 => [
                'name' => [
                    'Bank Pekao',
                ],
                'logo' => [
                    'Bank-Pekao-SA-191x30.png',
                ],
                'url' => [
                    'https://www.pekao.com.pl/'
                ],
                'contact_file' => [
                    ''
                ]
            ],
            2 => [
                'name' => [
                    'Bank Zachodni WBK',
                ],
                'logo' => [
                    'Bank_Zachodni_WBK_-_logo.png',
                ],
                'url' => [
                    'https://www.bzwbk.pl/'
                ],
                'contact_file' => [
                    ''
                ]
            ],
            3 => [
                'name' => [
                    'mBank',
                ],
                'logo' => [
                    'Mbank-logo.jpg',
                ],
                'url' => [
                    'https://www.mbank.pl/indywidualny/'
                ],
                'contact_file' => [
                    ''
                ]
            ],
            4 => [
                'name' => [
                    'ING Bank Śląski',
                ],
                'logo' => [
                    'ING_logo.png',
                ],
                'url' => [
                    'https://www.ingbank.pl/'
                ],
                'contact_file' => [
                    ''
                ]
            ],
            5 => [
                'name' => [
                    'Bank BGŻ BNP Paribas',
                ],
                'logo' => [
                    'BNP_Paribas.png',
                ],
                'url' => [
                    'https://www.bgzbnpparibas.pl/klienci-indywidualni'
                ],
                'contact_file' => [
                    ''
                ]
            ],
            6 => [
                'name' => [
                    'Bank Millennium',
                ],
                'logo' => [
                    'MillenniumCMYK.jpg',
                ],
                'url' => [
                    'https://www.bankmillennium.pl/'
                ],
                'contact_file' => [
                    ''
                ]
            ],
            7 => [
                'name' => [
                    'Getin Noble Bank',
                ],
                'logo' => [
                    'getin_logo.png',
                ],
                'url' => [
                    'http://gnb.pl/'
                ],
                'contact_file' => [
                    ''
                ]
            ],
            8 => [
                'name' => [
                    'Alior Bank',
                ],
                'logo' => [
                    'aliorbank_logo.png',
                ],
                'url' => [
                    'https://www.aliorbank.pl/'
                ],
                'contact_file' => [
                    ''
                ]
            ],
            9 => [
                'name' => [
                    'Raiffeisen Polbank',
                ],
                'logo' => [
                    'raiffeisen_logo.png',
                ],
                'url' => [
                    'https://raiffeisenpolbank.com/'
                ],
                'contact_file' => [
                    ''
                ]
            ],
            10 => [
                'name' => [
                    'Bank Handlowy',
                ],
                'logo' => [
                    'ciit_logo_bt.gif',
                ],
                'url' => [
                    'http://www.citibank.pl'
                ],
                'contact_file' => [
                    ''
                ]
            ],
            11 => [
                'name' => [
                    'Deutsche Bank',
                ],
                'logo' => [
                    'Deutsche_Bank-Logo.svg',
                ],
                'url' => [
                    'https://www.deutschebank.pl/'
                ],
                'contact_file' => [
                    ''
                ]
            ],
            12 => [
                'name' => [
                    'Idea Bank',
                ],
                'logo' => [
                    'Idea_Bank_logo.svg',
                ],
                'url' => [
                    'https://www.ideabank.pl/'
                ],
                'contact_file' => [
                    ''
                ]
            ],
            13 => [
                'name' => [
                    'Bank Ochrony Środowiska',
                ],
                'logo' => [
                    'logo-bosbank.png',
                ],
                'url' => [
                    'https://www.bosbank.pl/'
                ],
                'contact_file' => [
                    ''
                ]
            ],
            14 => [
                'name' => [
                    'Credit Agricole Bank',
                ],
                'logo' => [
                    'Logo_Credit_Agricole.png',
                ],
                'url' => [
                    'https://www.credit-agricole.pl/klienci-indywidualni'
                ],
                'contact_file' => [
                    ''
                ]
            ],
            15 => [
                'name' => [
                    'Bank Polskiej Spółdzielczości',
                ],
                'logo' => [
                    'bps_logo.png',
                ],
                'url' => [
                    'https://www.bankbps.pl/'
                ],
                'contact_file' => [
                    ''
                ]
            ],
            16 => [
                'name' => [
                    'Santander Consumer Bank',
                ],
                'logo' => [
                    'Banco_Santander_Logotipo.svg',
                ],
                'url' => [
                    'https://www.santanderconsumer.pl/'
                ],
                'contact_file' => [
                    ''
                ]
            ],
            17 => [
                'name' => [
                    'SGB-Bank',
                ],
                'logo' => [
                    'gbw_logo_new.gif',
                ],
                'url' => [
                    'http://sgbbank.com.pl/'
                ],
                'contact_file' => [
                    ''
                ]
            ],
            18 => [
                'name' => [
                    'Euro Bank',
                ],
                'logo' => [
                    'eurobank_logo.png',
                ],
                'url' => [
                    'https://www.eurobank.pl/'
                ],
                'contact_file' => [
                    ''
                ]
            ],
            19 => [
                'name' => [
                    'DNB Bank Polska',
                ],
                'logo' => [
                    'dnb_logo.png',
                ],
                'url' => [
                    'https://www.dnb.pl/'
                ],
                'contact_file' => [
                    ''
                ]
            ],
            20 => [
                'name' => [
                    'Bank Pocztowy',
                ],
                'logo' => [
                    'bank-pocztowy-logo.png',
                ],
                'url' => [
                    'https://www.pocztowy.pl/indywidualni/'
                ],
                'contact_file' => [
                    ''
                ]
            ],
            21 => [
                'name' => [
                    'Plus Bank',
                ],
                'logo' => [
                    'plusbank_logo.png',
                ],
                'url' => [
                    'https://plusbank.pl/'
                ],
                'contact_file' => [
                    ''
                ]
            ],
            22 => [
                'name' => [
                    'Nest Bank',
                ],
                'logo' => [
                    'nestbank_logo.png',
                ],
                'url' => [
                    'https://www.nestbank.pl/'
                ],
                'contact_file' => [
                    ''
                ]
            ],
        ];

        foreach ($lists as $i => $item) {
            $bank = new Bank();
            $bank->browser_id = 1;
            $bank->name = $item['name'][0];
            $bank->logo = $item['logo'][0];
            $bank->url = $item['url'][0];
            $bank->save();
        }
    }
}
