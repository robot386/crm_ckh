<?php

use Illuminate\Database\Seeder;
use App\Valuation;

class ValuationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $lists = [
            'rzeczoznawca doradcy',
            'rzeczoznawca klienta',
            'inne',
        ];

        foreach ($lists as $item) {
            $val = new Valuation();
            $val->name = $item;
            $val->save();
        }
    }
}
