<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(PurposesTableSeeder::class);
        $this->call(GaragesTableSeeder::class);
        $this->call(ValuationsTableSeeder::class);
        $this->call(BrowsersTableSeeder::class);
        $this->call(ModePurchasesTableSeeder::class);
        $this->call(BanksTableSeeder::class);
        $this->call(ProcesesTableSeeder::class);
//        $this->call(CustomersTableSeeder::class);
//        $this->call(PropertiesTableSeeder::class);
//        $this->call(PurchasesTableSeeder::class);
//        $this->call(CreditTableSeeder::class);
//        $this->call(CustomerProcessTableSeeder::class);

    }
}
