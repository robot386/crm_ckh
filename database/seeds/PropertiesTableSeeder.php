<?php

use Illuminate\Database\Seeder;
use App\Property;

class PropertiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('pl_PL');

        foreach (range(1, 50) as $index) {
            $property = new Property();
            $property->customer_id = $index;
            $property->garage_id = $faker->numberBetween(1, 3);
            $property->valuation_id = $faker->numberBetween(1, 3);
            $property->land_register = (int) $faker->boolean;
            $property->plot_number = $faker->swiftBicNumber;
            $property->repairs_price = $faker->randomNumber(6);
            $property->save();
        }
    }
}
