<?php

use Illuminate\Database\Seeder;
use App\ModePurchase;

class ModePurchasesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $lists = [
            'jednorazowy',
            'ratalny',
            'inny',
        ];

        foreach ($lists as $item) {
            $mode = new ModePurchase();
            $mode->name = $item;
            $mode->save();
        }
    }
}
