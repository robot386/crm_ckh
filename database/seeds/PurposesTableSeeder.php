<?php

use Illuminate\Database\Seeder;
use App\Purpose;

class PurposesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $lists = [
            'zakup domu z rynku pierwotnego',
            'zakup domu z rynku wtórnego',
            'zakup mieszkania z rynku pierwotnego',
            'zakup mieszkania z rynku wtórnego',
            'zakup działki budowlanej',
            'zakup domu letniskowego',
            'zakup działki rekreacyjnej',
            'budowa domu',
            'budowa mieszkania',
            'modernizacja',
            'remont',
            'wykończenie nieruchomości',
            'dostosowanie budynku do celów mieszkalnych',
            'budowa garażu',
            'zakup garażu',
            'wykup nieruchomości od gminy',
            'wykup nieruchomości od spółdzielni mieszkaniowej',
            'wykup nieruchomości od skarbu państwa',
            'wykup nieruchomości od syndyka',
            'spłata innego kredytu hipotecznego',
        ];

        foreach ($lists as $item) {
            $porpose = new Purpose();
            $porpose->name = $item;
            $porpose->save();
        }

    }
}
