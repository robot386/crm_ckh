<?php

use Illuminate\Database\Seeder;
use App\Process;
use App\Bank;

class ProcesesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $lists = [
            'złożenie wniosku',
            'decyzja kredytowa wstępna',
            'wycena nieruchomości',
            'decyzja kredytowa ostateczna',
            'umowa kredytowa',
            'wyplata kredytu',
        ];

        $banks = Bank::count();

        for ($i = 1; $i <= $banks; $i++) {
            foreach ($lists as $k => $item) {
                $process = new Process();
                $process->bank_id = $i;
                $process->name = $item;
                $process->order = $k;
                $process->save();
            }
        }
    }
}
