<?php

use Illuminate\Database\Seeder;
use App\Purchase;

class PurchasesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('pl_PL');

        foreach (range(1, 50) as $index) {
            $purchase = new Purchase();
            $purchase->customer_id = $index;
            $purchase->mode_purchase_id = $faker->numberBetween(1, 3);
            $purchase->price = $faker->randomNumber(6);
            $purchase->save();
        }
    }
}
