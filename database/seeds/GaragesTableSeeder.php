<?php

use Illuminate\Database\Seeder;
use App\Garage;

class GaragesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $lists = [
            'mieszkanie bez garażu',
            'mieszkanie z garażem',
            'mieszkanie z garażem poza kredytem',
        ];

        foreach ($lists as $item) {
            $garage = new Garage();
            $garage->name = $item;
            $garage->save();
        }
    }
}
