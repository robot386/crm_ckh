<?php

use Illuminate\Database\Seeder;
use App\Browser;

class BrowsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $lists = [
            'Windows' => [
                'IE' => 'C:\Program Files\Internet Explorer\iexplore.exe',
                'Firefox' => 'C:\Program Files\Mozilla Firefox\firefox.exe',
                'Chrome' => 'C:\Program Files\Google\Chrome\Application\chrome.exe',
                'Opera' => 'C:\Program Files\Opera\Launcher.exe',
            ],
            'OS X' => [
                'Brave' => '/Applications/Brave.app',
                'Chrome' => '/Applications/Google\ Chrome.app',
                'Opera' => '/Applications/Opera.app',
                'Safari' => '/Applications/Safari.app',
                'Waterfox' => '/Volumes/mgz/Applications/Waterfox.app',
                'Maxthon' => '/Volumes/mgz/Applications/Maxthon.app'
            ],
            'Linux' => [],
        ];

        foreach ($lists as $os => $array) {
            foreach ($array as $browserName => $path) {
                $browser = new Browser();
                $browser->name = $browserName;
                $browser->path = $path;
                $browser->os = $os;
                $browser->save();
            }
        }
    }
}
