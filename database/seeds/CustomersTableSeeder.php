<?php

use Illuminate\Database\Seeder;
use App\Customer;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('pl_PL');

        foreach (range(1, 50) as $index) {
            $customer = new Customer();
//            $customer->credit_id = $index;
            $customer->name = $faker->lastName . ' ' . $faker->firstName;
            $customer->phone = $faker->phoneNumber;
            $customer->email = $faker->email;
            $customer->address = $faker->address;
            $customer->next_contract_date = $faker->dateTime;
            $customer->source_customer_description = $faker->realText(200);
            $customer->save();
        }
    }
}
