<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('browser_id')->unsigned()->nullable();
            $table->string('name');
            $table->string('logo')->nullable();
            $table->string('url');
            $table->string('contact_file')->nullable();
            $table->timestamps();

            $table->foreign('browser_id')
                ->references('id')
                ->on('browsers')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banks');
    }
}
