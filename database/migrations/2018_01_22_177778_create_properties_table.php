<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->nullable()->unsigned()->comment('klient');
            $table->integer('garage_id')->nullable()->unsigned()->comment('informacja odnośnie garażu');
            $table->integer('valuation_id')->nullable()->unsigned()->comment('informacja odnośnie wyceny nieruchomości');
            $table->tinyInteger('land_register')->default(0)->comment('księga wieczysta');
            $table->string('plot_number')->nullable()->comment('numer działki');
            $table->decimal('repairs_price', 22, 4)->nullable()->comment('koszt wykończenia/remontu');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('customer_id')
                ->references('id')
                ->on('customers');

            $table->foreign('garage_id')
                ->references('id')
                ->on('garages')
                ->onDelete('cascade');

            $table->foreign('valuation_id')
                ->references('id')
                ->on('valuations')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
