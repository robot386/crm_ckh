<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credits', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->nullable()->unsigned()->comment('klient');
            $table->integer('purpose_id')->nullable()->unsigned()->comment('cel kredytu');
            $table->decimal('amount', 22, 4)->nullable()->comment('kwota kredytu');
            $table->decimal('amount_own', 22, 4)->nullable()->comment('wkład własny');
            $table->text('description')->nullable()->comment('opis kredytu');
            $table->text('property_description')->nullable()->comment('informacja o celu kredytu');
            $table->dateTime('payment_date')->nullable()->comment('data wypłaty kredytu');
            $table->string('loan_period')->nullable()->comment('okres kredytowania');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('customer_id')
                ->references('id')
                ->on('customers');

            $table->foreign('purpose_id')
                ->references('id')
                ->on('purposes')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credits');
    }
}
