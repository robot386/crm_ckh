<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerProcessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_processes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bank_id')->unsigned()->nullable();
            $table->integer('bank_prefer')->unsigned()->nullable();
            $table->integer('customer_id')->unsigned()->nullable();
            $table->string('name')->nullable();
            $table->tinyInteger('order')->default(0);
            $table->tinyInteger('status')->default(1);
            $table->tinyInteger('active')->default(0);
            $table->dateTime('date')->nullable();
            $table->string('color')->nullable();
            $table->timestamps();

            $table->foreign('bank_id')
                ->references('id')
                ->on('banks')
                ->onDelete('cascade');

            $table->foreign('customer_id')
                ->references('id')
                ->on('customers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_processes');
    }
}
