<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable()->unsigned()->comment('użytkownik');
            $table->integer('customer_id')->nullable()->unsigned()->comment('współkredytobiorca');
            $table->string('name');
            $table->string('phone')->nullable();
            $table->string('email');
            $table->string('address')->nullable();
            $table->tinyInteger('active')->default(1);

            // umowa
            $table->dateTime('next_contract_date')->nullable()->comment('data następnej umowy');
            $table->text('source_customer_description')->nullable()->comment('źródło pozyskania klienta');

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('customer_id')
                ->references('id')
                ->on('customers')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
